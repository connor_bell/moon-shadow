//
//  SocialUtil.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-07-24.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocialUtil : NSObject

+ (void)openUserInstagram:(NSString *)username;
+ (void)openUserTwitter:(NSString *)username;

@end
