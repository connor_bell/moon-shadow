//
//  UIHelper.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-26.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString *NOTIFICATION_BACKGROUND_ENTER;
extern NSString *NOTIFICATION_BACKGROUND_EXIT;

@interface UIHelper : NSObject

+ (UIColor *)opaqueBackgroundColor;
+ (UIColor *)transparentBackgroundColor;
+ (UIImage *)rotateUIImage:(UIImage*)sourceImage dir:(UIDeviceOrientation)devOrientation;
+ (UIColor *)interpolate:(UIColor *)c1 c2:(UIColor *)c2 t:(float)t;
+ (BOOL)highQualityVideoSupported;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height;

@end
