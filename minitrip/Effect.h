//
//  Effect.h
//  minitrip
//
//  Created by Connor Bell on 2015-04-12.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPUImageFilter.h"

#define EFFECT_START_VAL 0.25

typedef enum {
    BlendTypeNormal = 0,
    BlendTypeMultiply = 1,
    BlendTypeReplace = 2
} BlendType;

@interface Effect : NSObject

@property (nonatomic, readonly) UIImage *previewImage;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readwrite) BlendType blendType;
@property (nonatomic, strong) UIColor *currentColor;
@property (nonatomic, readwrite) CGPoint touchPos;

- (void)updateSensitivityWithValue:(float)value;
- (void)updateTimewithValue:(float)value;

- (float)getIntensity;

- (GPUImageFilter *)imageFilter;

@end
