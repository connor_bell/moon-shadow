//
//  HueyEffect.m
//  minitrip
//
//  Created by Connor Bell on 2015-04-19.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "HueyEffect.h"
#import "HueyFilter.h"

@interface HueyEffect () {
    HueyFilter *_imageFilter;
}

@end

@implementation HueyEffect
@synthesize currentColor=_currentColor;
@synthesize blendType=_blendType;
- (instancetype)init {
    if (self = [super init]) {
        _imageFilter = [HueyFilter new];
        [_imageFilter setSensitivity:EFFECT_START_VAL];
    }
    return self;
}

- (NSString *)title {
    return @"HUEY";
}

- (UIImage *)previewImage {
    return [UIImage imageNamed:@"HUEY"];
}

- (void)updateTimewithValue:(float)value {
    [_imageFilter setTime:value];
}

- (void)updateSensitivityWithValue:(float)value {
    [_imageFilter setSensitivity:value];
}

- (void)setCurrentColor:(UIColor *)currentColor {
    _currentColor = currentColor;
    [_imageFilter setColorBlendValue:_currentColor];
}

- (GPUImageFilter *)imageFilter {
    return _imageFilter;
}

- (float)getIntensity {
    return _imageFilter.sensitivity;
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [_imageFilter setBlendType:blendType];
}

- (void)setTouchPos:(CGPoint)touchPos {
    _imageFilter.touchX = touchPos.x;
    _imageFilter.touchY = touchPos.y;
}

@end
