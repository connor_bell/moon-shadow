#import "SunshineFilter.h"

NSString *const kGPUImageSunshineFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform lowp vec3 colorBlend;
 uniform highp float time;
 uniform lowp float sensitivity;
 uniform lowp float intensity;
 uniform lowp int blendType;
 uniform lowp float touchX;
 uniform lowp float touchY;
 uniform highp float resX;
 uniform highp float resY;
 
 highp float lum(lowp vec3 c) {
     return dot(c, vec3(0.3, 0.59, 0.11));
 }
 
 lowp vec3 clipcolor(lowp vec3 c) {
     highp float l = lum(c);
     lowp float n = min(min(c.r, c.g), c.b);
     lowp float x = max(max(c.r, c.g), c.b);
     
     if (n < 0.0) {
         c.r = l + ((c.r - l) * l) / (l - n);
         c.g = l + ((c.g - l) * l) / (l - n);
         c.b = l + ((c.b - l) * l) / (l - n);
     }
     if (x > 1.0) {
         c.r = l + ((c.r - l) * (1.0 - l)) / (x - l);
         c.g = l + ((c.g - l) * (1.0 - l)) / (x - l);
         c.b = l + ((c.b - l) * (1.0 - l)) / (x - l);
     }
     
     return c;
 }
 
 lowp vec3 setlum(lowp vec3 c, highp float l) {
     highp float d = l - lum(c);
     c = c + vec3(d);
     return clipcolor(c);
 }
 
 lowp vec3 rgb2hsv(lowp vec3 c)
{
    lowp vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    lowp vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    lowp vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
    
    lowp float d = q.x - min(q.w, q.y);
    lowp float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}
 
 lowp vec3 hsv2rgb(lowp vec3 c)
{
    lowp vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    lowp vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}
 
 highp float hash(highp float n )
{
    return fract(sin(n)*43758.5453123);
}
 
 highp float noise( lowp vec2 x )
{
    highp vec2 p = floor(x);
    highp vec2 f = fract(x);
    
    f = f*f*(3.0-2.0*f);
    
    highp float n = p.x + p.y*157.0;
    
    return mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
               mix( hash(n+157.0), hash(n+158.0),f.x),f.y);
}
 void main() {
     
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     highp vec2 touch = vec2(touchX, touchY);
     highp vec2 iResolution = vec2(resX,resY);
     highp vec2 fragCoord = textureCoordinate * iResolution.xy;

     lowp float i = textureCoordinate.x;
     lowp float j = textureCoordinate.y;
     
     highp vec2 v1 = vec2(250.0*abs(touch.x-i)*abs(touch.y-j)+time*4.0, j);
     highp vec2 v2 = vec2(250.0*abs(touch.x-i)*abs(touch.y-j)+time*4.0, j);
     
     highp float b = noise(v1);
     highp float r = noise(v2);
     
     highp vec4 ok = vec4(r, ((2.0-j)) - (r+b), r, 1.0);
     
     lowp float maskY = 0.2989 * colorBlend.r + 0.5866 * colorBlend.g + 0.1145 * colorBlend.b;
     lowp float maskCr = 0.7132 * (colorBlend.r - maskY);
     lowp float maskCb = 0.5647 * (colorBlend.b - maskY);
     
     lowp float Y = 0.2989 * textureColor.r + 0.5866 * textureColor.g + 0.1145 * textureColor.b;
     lowp float Cr = 0.7132 * (textureColor.r - Y);
     lowp float Cb = 0.5647 * (textureColor.b - Y);
     
     lowp float blendValue = 1.0 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), vec3(maskY,maskCr, maskCb)));
     
     if (blendType == 0) {
         ok = vec4(textureColor.rgb * (1.0 - ok.a) + setlum(ok.rgb, lum(textureColor.rgb)) * ok.a, textureColor.a);
     }
     if (blendType == 1) {
         ok = ok * textureColor + ok * (1.0 - textureColor.a) + textureColor * (1.0 - ok.a);
     }
     
     gl_FragColor = mix(textureColor, ok, blendValue);
 }
 
 );


@interface SunshineFilter () {
    GLint colorBlendUniform;
    GLint timeUniform;
    GLint intensityUniform;
    GLint sensitivityUniform;
    GLint blendTypeUniform;
    GLint touchXUniform;
    GLint touchYUniform;
    GLint resXUniform;
    GLint resYUniform;
}

@end

@implementation SunshineFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageSunshineFragmentShaderString])) {
        return nil;
    }
    
    colorBlendUniform = [filterProgram uniformIndex:@"colorBlend"];
    timeUniform = [filterProgram uniformIndex:@"time"];
    intensityUniform = [filterProgram uniformIndex:@"intensity"];
    sensitivityUniform = [filterProgram uniformIndex:@"sensitivity"];
    blendTypeUniform = [filterProgram uniformIndex:@"blendType"];
    
    touchXUniform = [filterProgram uniformIndex:@"touchX"];
    touchYUniform = [filterProgram uniformIndex:@"touchY"];
    resXUniform = [filterProgram uniformIndex:@"resX"];
    resYUniform = [filterProgram uniformIndex:@"resY"];
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    [self setFloat:(float)bounds.size.width forUniform:resXUniform program:filterProgram];
    [self setFloat:(float)bounds.size.height forUniform:resYUniform program:filterProgram];
    
    return self;
}

- (void)setTime:(float)t {
    _time = t;
    [self setFloat:t forUniform:timeUniform program:filterProgram];
}

- (void)setColorBlendValue:(UIColor *)colorBlendValue {
    _colorBlendValue = colorBlendValue;
    
    CGFloat r, g, b, a;
    [colorBlendValue getRed:&r green:&g blue:&b alpha:&a];
    
    GPUVector3 c;
    c.one = r;
    c.two = g;
    c.three = b;
    
    [self setVec3:c forUniform:colorBlendUniform program:filterProgram];
}

- (void)setIntensity:(float)intensity {
    _intensity = intensity;
    [self setFloat:intensity forUniform:intensity program:filterProgram];
}

- (void)setSensitivity:(float)sensitivity {
    _sensitivity = sensitivity;
    [self setFloat:_sensitivity forUniform:sensitivityUniform program:filterProgram];
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [self setInteger:_blendType forUniform:blendTypeUniform program:filterProgram];
}

- (void)setTouchX:(float)touchX {
    _touchX = touchX;
    [self setFloat:_touchX forUniform:touchXUniform program:filterProgram];
}

- (void)setTouchY:(float)touchY {
    _touchY = touchY;
    [self setFloat:_touchY forUniform:touchYUniform program:filterProgram];
}

@end
