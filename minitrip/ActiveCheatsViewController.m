//
//  ActiveCheatsViewController.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-28.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "ActiveCheatsViewController.h"
#import "UnlockedCheatTableViewCell.h"
#import "CheatStore.h"

static NSString *SEGUE_ID_ENTER_CHEAT = @"SEGUE_ID_ENTER_CHEAT";

@interface ActiveCheatsViewController () {
    NSArray *_cheats;
}

@end

@implementation ActiveCheatsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UnlockedCheatTableViewCell class]
           forCellReuseIdentifier:@"UnlockedCheatTableViewCell"];
    [self.tableView reloadData];
    self.title = @"MOON SHADOW";
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _cheats = [[CheatStore sharedInstance] getUnlockedCheats];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        static NSString *tableViewCellId = @"TableViewCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tableViewCellId forIndexPath:indexPath];
        
        cell.textLabel.text = @"ENTER CHEAT";
        
        return cell;
        
    } else {
        static NSString *cellId = @"UnlockedCheatTableViewCell";
        UnlockedCheatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
        
        cell.titleLabel.text = [_cheats[indexPath.row] title];
        [cell.unlockedSwitch setOn:[_cheats[indexPath.row] isEnabled]];
        cell.cheat = _cheats[indexPath.row];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self performSegueWithIdentifier:SEGUE_ID_ENTER_CHEAT sender:nil];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) return 1;
    else return [_cheats count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

@end
