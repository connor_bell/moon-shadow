//
//  EnterCheatViewController.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-28.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "EnterCheatViewController.h"
#import "CheatStore.h"
#import "HotPavementEffect.h"

@interface EnterCheatViewController () {
    UIImage *_inputImage;
    GPUImagePicture *_stillImageSource;
    HotPavementEffect *_falseGodEffect;
    GPUImageFilter *_emptyImageFilter;
    NSTimer *_effectTimer;
    NSDate *_startDate;
    int _attempts;
}

@end

@implementation EnterCheatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _inputImage = [UIImage imageNamed:@"Icon-Transparent.png"];
    _attempts = 0;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _stillImageSource = [[GPUImagePicture alloc] initWithImage:_inputImage];
    _emptyImageFilter = [GPUImageFilter new];
    [_stillImageSource addTarget:_emptyImageFilter];
    [_emptyImageFilter addTarget:_gpuImageView];
    [_emptyImageFilter useNextFrameForImageCapture];
    [_stillImageSource processImage];
    [_codeTextField becomeFirstResponder];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self invalidateTimer];
    [_codeTextField resignFirstResponder];
}

- (IBAction)enterTapped:(id)sender {
    Cheat *cheat = [[CheatStore sharedInstance] codeUnlocksCheat:_codeTextField.text];
    
    if (cheat) {
        [self showCodeSuccess:cheat];
    } else {
        [self showCodeFailed];
    }
}

- (void)invalidateTimer {
    if (_effectTimer) {
        [_effectTimer invalidate];
        _effectTimer = nil;
    }
}

- (void)showCodeSuccess:(Cheat *)cheat {
    UIAlertView *successView = [[UIAlertView alloc] initWithTitle:@"CHEAT UNLOCKED" message:[NSString stringWithFormat:@"You unlocked %@", cheat.title] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Cool!", nil];
    [successView show];

    if (!_falseGodEffect) {

        [self invalidateTimer];
        _falseGodEffect = [HotPavementEffect new];
        [_falseGodEffect updateSensitivityWithValue:2.0];
        [_stillImageSource removeAllTargets];
        [_stillImageSource addTarget:[_falseGodEffect imageFilter]];
        [[_falseGodEffect imageFilter] addTarget:_gpuImageView];
        
        _startDate = [NSDate date];
        _effectTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0/20.0)
                                                        target:self
                                                      selector:@selector(updateTime)
                                                      userInfo:nil repeats:YES];
        [_effectTimer fire];
    }
}

- (void)showCodeFailed {
    UIAlertView *failedView = [[UIAlertView alloc] initWithTitle:@"NOT A CODE" message:[self stringForAttempts] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK!", nil];
    [failedView show];
    _attempts++;
}

- (NSString *)stringForAttempts {
    if (_attempts == 0) {
        return @"SORRY PLEASE TRY AGAIN!";
    } else if (_attempts == 1) {
        return @"SORRY PLEASE KEEP TRYING!";
    } else if (_attempts == 2) {
        return @"SORRY THAT'S NOT A CODE. TRY \"random\"!";
    } else {
        return @"SORRY PLEASE TRY AGAIN!";
    }
}

- (void)updateTime {
    NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:_startDate];
    [_falseGodEffect updateTimewithValue:time];
    [[_falseGodEffect imageFilter] useNextFrameForImageCapture];
    [_stillImageSource processImage];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if ([string isEqualToString:@"\n"]) {
        [self enterTapped:nil];
        return NO;
    }
    return YES;
}


@end
