//
//  Effect.m
//  minitrip
//
//  Created by Connor Bell on 2015-04-12.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "Effect.h"

@implementation Effect
- (void)updateSensitivityWithValue:(float)value{}
- (void)updateTimewithValue:(float)value{}
- (float)getIntensity {return 0.0;}
- (void)updateColor:(UIColor *)color {
    
}
- (GPUImageFilter *)imageFilter {return nil;}
@end
