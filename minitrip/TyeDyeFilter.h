//
//  GradientFilter.h
//  minitrip
//
//  Created by Connor Bell on 2014-12-25.
//  Copyright (c) 2014 Connor Bell. All rights reserved.
//

#import "GPUImageFilter.h"
#import "Effect.h"

@interface TyeDyeFilter : GPUImageFilter

@property(readwrite, nonatomic) UIColor *colorBlendValue;
@property(readwrite, nonatomic) BlendType blendType;
@property(readwrite, nonatomic) float intensity;
@property(readwrite, nonatomic) float sensitivity;
@property(readwrite, nonatomic) float time;
@property(readwrite, nonatomic) float touchX;
@property(readwrite, nonatomic) float touchY;

@end
