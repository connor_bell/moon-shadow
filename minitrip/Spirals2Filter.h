//
//  Spirals2Filter.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-30.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "GPUImageFilter.h"
#import "Effect.h"

@interface Spirals2Filter : GPUImageFilter

@property(readwrite, nonatomic) UIColor *colorBlendValue;
@property(readwrite, nonatomic) BlendType blendType;
@property(readwrite, nonatomic) float intensity;
@property(readwrite, nonatomic) float sensitivity;
@property(readwrite, nonatomic) float time;
@property(readwrite, nonatomic) float touchX;
@property(readwrite, nonatomic) float touchY;

@end
