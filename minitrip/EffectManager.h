//
//  EffectManager.h
//  minitrip
//
//  Created by Connor Bell on 2015-04-12.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EffectManager : NSObject

+ (EffectManager *)sharedInstance;

@property (nonatomic, readonly) NSMutableArray *effects;

- (void)modifyEffectsForActiveCheats;

@end
