//
//  InfoView.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-27.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "InfoView.h"
#import <QuartzCore/QuartzCore.h>

static NSString *DEFAULTS_KEY_TUTORIAL_SHOWN = @"DEFAULTS_KEY_TUTORIAL_SHOWN";

@interface InfoView () {
    BOOL _isShowingTutorial;
}

@end

@implementation InfoView

- (void)showIfFirstLaunch {
    _shareButton.hidden = YES;
    _previewButton.hidden = YES;
    _arrowImageView.hidden = YES;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:DEFAULTS_KEY_TUTORIAL_SHOWN]) {
        self.hidden = YES;
        _isShowingTutorial = NO;
    } else {
        self.hidden = NO;
        _isShowingTutorial = YES;
        _infoLabel.text = @"WELCOME, FRIEND! Try tapping an effect.";
    }
}

- (void)action:(InfoViewAction)act {
    if (self.hidden || !_isShowingTutorial) return;
    if (act == InfoViewEffectSelect) {
        _infoLabel.text = @"Sweet! Now, tap on the color you want to blend with";
    }
    if (act == InfoViewImageTapped) {
        _infoLabel.text = @"Good job! Your color is displayed below. Adjust parameters by tapping it.";
        _arrowImageView.hidden = NO;
    }
    if (act == InfoViewSensitivityTapped) {
        _infoLabel.text = @"GOOD LUCK!";
        dispatch_time_t startTime = dispatch_time(DISPATCH_TIME_NOW, 2.25 * NSEC_PER_SEC);
        dispatch_after(startTime, dispatch_get_main_queue(), ^(void){
            [UIView animateWithDuration:0.25 animations:^{
                self.alpha = 0.0f;
            } completion:^(BOOL finished) {
                self.hidden = YES;
                _arrowImageView.hidden = YES;
                _isShowingTutorial = NO;
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DEFAULTS_KEY_TUTORIAL_SHOWN];
            }];
        });
    }
}

- (void)showImageSaved {
    _latestSaveVideo = NO;
    _infoLabel.text = @"IMAGE SAVED";
    _previewButton.hidden = NO;
    _shareButton.hidden = NO;
    [self show];
}

- (void)showVideoSaved {
    _latestSaveVideo = YES;
    _infoLabel.text = @"VIDEO SAVED";
    _shareButton.hidden = NO;
    _previewButton.hidden = YES;
    [self show];
}

- (void)show {
    self.hidden = NO;
    self.alpha = 0.0f;
    [self.layer removeAllAnimations];
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1.0f;
    } completion:^(BOOL finished) {
        dispatch_time_t startTime = dispatch_time(DISPATCH_TIME_NOW, 2.25 * NSEC_PER_SEC);
        dispatch_after(startTime, dispatch_get_main_queue(), ^(void){
            [UIView animateWithDuration:0.2 animations:^{
                self.alpha = 0.0f;
            } completion:^(BOOL finished) {
                self.hidden = YES;
            }];
        });
    }];
}

- (void)showError:(NSString *)errorMessage {
    _infoLabel.text = errorMessage;
    _shareButton.hidden = YES;
    _previewButton.hidden = YES;
    [self show];
}

@end
