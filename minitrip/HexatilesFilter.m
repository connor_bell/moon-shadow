#import "HexatilesFilter.h"

NSString *const kGPUImageHexatilesFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform lowp vec3 colorBlend;
 uniform highp float time;
 uniform lowp float sensitivity;
 uniform lowp float intensity;
 uniform lowp int blendType;
 uniform highp float resX;
 uniform highp float resY;
 
 mediump vec4 overlay(mediump vec4 base, mediump vec4 overlay) {
     mediump float ra;
     if (2.0 * base.r < base.a) {
         ra = 2.0 * overlay.r * base.r + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     } else {
         ra = overlay.a * base.a - 2.0 * (base.a - base.r) * (overlay.a - overlay.r) + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     }
     
     mediump float ga;
     if (2.0 * base.g < base.a) {
         ga = 2.0 * overlay.g * base.g + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     } else {
         ga = overlay.a * base.a - 2.0 * (base.a - base.g) * (overlay.a - overlay.g) + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     }
     
     mediump float ba;
     if (2.0 * base.b < base.a) {
         ba = 2.0 * overlay.b * base.b + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     } else {
         ba = overlay.a * base.a - 2.0 * (base.a - base.b) * (overlay.a - overlay.b) + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     }
     return vec4(ra, ga, ba, 1.0);
 }
 
 //hexatiles via @paniq  http://glsl.herokuapp.com/e#4841.11
 mediump float hexatiles(mediump vec2 d) {
     d.x *= 0.8660254037844386;
     mediump vec2 p = mod(d, vec2(3.0, 2.0));
     
     mediump vec2 p0 = abs(p - vec2(1.5, 1.0));
     mediump vec2 p1 = abs(p0 - vec2(1.5, 1.0));
     
     return min(max(p0.x + p0.y*0.5, p0.y),max(p1.x + p1.y*0.5, p1.y));
 }
 
 void main() {
     
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     
     mediump vec2 uv = textureCoordinate;
     uv = 2.0 * uv - 1.0;
     highp float t = time *0.5;
     uv.x *= resX / resY;
     
     mediump vec2 ms = vec2(0.95+sin(t*.7)*0.1,1.0);
     ms.x *= resX / resY;
     ms.y *= resX / resY;
     
     mediump float d1 = hexatiles(uv*ms*24.);
     mediump float d2 = hexatiles(-uv*ms*23.);
     mediump float d3 = hexatiles(uv*ms*25.);
     
     lowp vec4 c = vec4(d1 *sin(t*.8)+0.5, d2 *cos(t*.3), d3*-sin(t*.5), 1.0);
     
     lowp float maskY = 0.2989 * colorBlend.r + 0.5866 * colorBlend.g + 0.1145 * colorBlend.b;
     lowp float maskCr = 0.7132 * (colorBlend.r - maskY);
     lowp float maskCb = 0.5647 * (colorBlend.b - maskY);
     
     lowp float Y = 0.2989 * textureColor.r + 0.5866 * textureColor.g + 0.1145 * textureColor.b;
     lowp float Cr = 0.7132 * (textureColor.r - Y);
     lowp float Cb = 0.5647 * (textureColor.b - Y);
     
     lowp float blendValue = 1.0 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), vec3(maskY,maskCr, maskCb)));
     
     if (blendType == 0) {
         c = overlay(c, textureColor);
     }
     if (blendType == 1) {
         c = c * textureColor + c * (1.0 - textureColor.a) + textureColor * (1.0 - c.a);
     }
     
     gl_FragColor = mix(textureColor, c, blendValue);
 }
 
 );


@interface HexatilesFilter () {
    GLint colorBlendUniform;
    GLint timeUniform;
    GLint intensityUniform;
    GLint sensitivityUniform;
    GLint blendTypeUniform;
    GLint resXUniform;
    GLint resYUniform;
}

@end

@implementation HexatilesFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageHexatilesFragmentShaderString])) {
        return nil;
    }
    
    colorBlendUniform = [filterProgram uniformIndex:@"colorBlend"];
    timeUniform = [filterProgram uniformIndex:@"time"];
    intensityUniform = [filterProgram uniformIndex:@"intensity"];
    sensitivityUniform = [filterProgram uniformIndex:@"sensitivity"];
    blendTypeUniform = [filterProgram uniformIndex:@"blendType"];
    resXUniform = [filterProgram uniformIndex:@"resX"];
    resYUniform = [filterProgram uniformIndex:@"resY"];
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    [self setFloat:(float)bounds.size.width forUniform:resXUniform program:filterProgram];
    [self setFloat:(float)bounds.size.height forUniform:resYUniform program:filterProgram];
    
    return self;
}

- (void)setTime:(float)t {
    _time = t;
    [self setFloat:t forUniform:timeUniform program:filterProgram];
}

- (void)setColorBlendValue:(UIColor *)colorBlendValue {
    _colorBlendValue = colorBlendValue;
    
    CGFloat r, g, b, a;
    [colorBlendValue getRed:&r green:&g blue:&b alpha:&a];
    
    GPUVector3 c;
    c.one = r;
    c.two = g;
    c.three = b;
    
    [self setVec3:c forUniform:colorBlendUniform program:filterProgram];
}

- (void)setIntensity:(float)intensity {
    _intensity = intensity;
    [self setFloat:intensity forUniform:intensity program:filterProgram];
}

- (void)setSensitivity:(float)sensitivity {
    _sensitivity = sensitivity;
    [self setFloat:_sensitivity forUniform:sensitivityUniform program:filterProgram];
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [self setInteger:_blendType forUniform:blendTypeUniform program:filterProgram];
}

@end
