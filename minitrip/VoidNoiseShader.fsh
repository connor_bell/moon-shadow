#extension GL_OES_standard_derivatives : enable

varying highp vec2 textureCoordinate;

uniform sampler2D inputImageTexture;

uniform lowp vec3 colorBlend;
uniform highp float time;
uniform lowp float sensitivity;
uniform lowp float intensity;
uniform lowp int blendType;
uniform lowp float touchX;
uniform lowp float touchY;
uniform highp float resX;
uniform highp float resY;

highp float lum(lowp vec3 c) {
    return dot(c, vec3(0.3, 0.59, 0.11));
}

lowp vec3 clipcolor(lowp vec3 c) {
    highp float l = lum(c);
    lowp float n = min(min(c.r, c.g), c.b);
    lowp float x = max(max(c.r, c.g), c.b);
    
    if (n < 0.0) {
        c.r = l + ((c.r - l) * l) / (l - n);
        c.g = l + ((c.g - l) * l) / (l - n);
        c.b = l + ((c.b - l) * l) / (l - n);
    }
    if (x > 1.0) {
        c.r = l + ((c.r - l) * (1.0 - l)) / (x - l);
        c.g = l + ((c.g - l) * (1.0 - l)) / (x - l);
        c.b = l + ((c.b - l) * (1.0 - l)) / (x - l);
    }
    
    return c;
}

lowp vec3 setlum(lowp vec3 c, highp float l) {
    highp float d = l - lum(c);
    c = c + vec3(d);
    return clipcolor(c);
}


void main()
{
    lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
    
    highp vec2 touch = vec2(touchX, touchY);
    highp vec2 iResolution = vec2(resX,resY);
    highp vec2 fragCoord = textureCoordinate * iResolution.xy;
    
    fragCoord.xy-=touch*iResolution.xy;
    
    highp vec2 uv = (fragCoord.xy / iResolution.xx);
    highp vec4 c = vec4(1.0);
    highp vec3 a = vec3(atan(uv.y,uv.x));
    highp vec3 d = vec3(length(uv));
    a.g+=.025;
    a.b+=.05;
    
    //CONNOR - replace with 2 fun accel axis going from -1 - +1
    //vec2 accel = iMouse.xy/iResolution.xy*2.-1.;
    lowp vec2 accel = vec2(0.5, 0.5);
    
    a+=accel.y*d;
    highp vec3 h = d*.5-accel.x*.5;
    highp vec3 coord = h*.5+sin(a*50.0*sin(a*3.0+time)*50.0)*d;
    c.rgb = abs(fract(coord-0.5)-0.5)/fwidth(coord*1.0);
    c.rgb = 1.0-min(c.rgb,1.0);
    
    lowp float maskY = 0.2989 * colorBlend.r + 0.5866 * colorBlend.g + 0.1145 * colorBlend.b;
    lowp float maskCr = 0.7132 * (colorBlend.r - maskY);
    lowp float maskCb = 0.5647 * (colorBlend.b - maskY);
    
    lowp float Y = 0.2989 * textureColor.r + 0.5866 * textureColor.g + 0.1145 * textureColor.b;
    lowp float Cr = 0.7132 * (textureColor.r - Y);
    lowp float Cb = 0.5647 * (textureColor.b - Y);
    
    lowp float blendValue = 1.0 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), vec3(maskY,maskCr, maskCb)));
    
    if (blendType == 0) {
        c = vec4(textureColor.rgb * (1.0 - c.a) + setlum(c.rgb, lum(textureColor.rgb)) * c.a, textureColor.a);
    } else if (blendType == 1) {
        c = c * textureColor + c * (1.0 - textureColor.a) + textureColor * (1.0 - c.a);
    } else {
        
    }
    
    gl_FragColor = mix(textureColor, c, blendValue);
}