//
//  CreditsViewController.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-05-06.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditsViewController : UITableViewController

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIView *creditsView;

- (IBAction)twitterButtonTapped:(id)sender;
- (IBAction)glitchWizardTapped:(id)sender;

@end
