//
//  LoadingOverlayView.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-23.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingOverlayView : UIView

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *indicatorView;

- (void)show;
- (void)hide;

@end
