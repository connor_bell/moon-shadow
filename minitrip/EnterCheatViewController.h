//
//  EnterCheatViewController.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-28.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoView.h"
#import "GPUImage.h"

@interface EnterCheatViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *codeTextField;
@property (nonatomic, strong) IBOutlet GPUImageView *gpuImageView;
@property (nonatomic, strong) IBOutlet InfoView *infoView;

- (IBAction)enterTapped:(id)sender;

@end
