//
//  FalseGodFilter.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-05-05.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "GPUImageFilter.h"
#import "Effect.h"

@interface FalseGodFilter : GPUImageFilter

@property(readwrite, nonatomic) UIColor *colorBlendValue;
@property(readwrite, nonatomic) BlendType blendType;
@property(readwrite, nonatomic) float intensity;
@property(readwrite, nonatomic) float sensitivity;
@property(readwrite, nonatomic) float time;

@end
