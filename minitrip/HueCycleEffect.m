#import "HueCycleEffect.h"
#import "HueCycleFilter.h"

@interface HueCycleEffect () {
    HueCycleFilter *_imageFilter;
}
@end

@implementation HueCycleEffect
@synthesize currentColor=_currentColor;
@synthesize blendType=_blendType;

- (instancetype)init {
    if (self = [super init]) {
        _imageFilter = [HueCycleFilter new];
        [_imageFilter setSensitivity:EFFECT_START_VAL];
    }
    return self;
}

- (NSString *)title {
    return @"GLITCHWIZ";
}

- (UIImage *)previewImage {
    return [UIImage imageNamed:@"GLITCHWIZ"];
}

- (void)updateTimewithValue:(float)value {
    [_imageFilter setTime:value];
}

- (void)updateSensitivityWithValue:(float)value {
    [_imageFilter setSensitivity:value];
}

- (void)setCurrentColor:(UIColor *)currentColor {
    _currentColor = currentColor;
    [_imageFilter setColorBlendValue:_currentColor];
}

- (GPUImageFilter *)imageFilter {
    return _imageFilter;
}

- (float)getIntensity {
    return _imageFilter.sensitivity;
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [_imageFilter setBlendType:blendType];
}

@end
