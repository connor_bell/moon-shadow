//
//  PreviewImageViewController.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-05-01.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface PreviewImageViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) UIImage *previewImage;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UIScrollView *imageScrollView;
@property (nonatomic, assign) ViewController *mainViewController;

- (IBAction)doneTapped:(id)sender;

@end
