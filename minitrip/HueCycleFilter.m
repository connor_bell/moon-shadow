#import "HueCycleFilter.h"

NSString *const kGPUImageHueCycleFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform lowp vec3 colorBlend;
 uniform highp float time;
 uniform lowp float sensitivity;
 uniform lowp float intensity;
 uniform lowp int blendType;
 
 lowp vec4 hue(lowp vec4 color, highp float shift) {
     
     const lowp vec4 kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
     const lowp vec4 kRGBToI     = vec4 (0.596, -0.275, -0.321, 0.0);
     const lowp vec4 kRGBToQ     = vec4 (0.212, -0.523, 0.311, 0.0);
     
     const lowp vec4 kYIQToR   = vec4 (1.0, 0.956, 0.621, 0.0);
     const lowp vec4 kYIQToG   = vec4 (1.0, -0.272, -0.647, 0.0);
     const lowp vec4 kYIQToB   = vec4 (1.0, -1.107, 1.704, 0.0);
     
     // Convert to YIQ
     lowp float   YPrime  = dot (color, kRGBToYPrime);
     lowp float   I      = dot (color, kRGBToI);
     lowp float   Q      = dot (color, kRGBToQ);
     
     // Calculate the hue and chroma
     lowp float   hue     = atan (Q, I);
     lowp float   chroma  = sqrt (I * I + Q * Q);
     
     // Make the user's adjustments
     hue += shift;
     
     // Convert back to YIQ
     Q = chroma * sin (hue);
     I = chroma * cos (hue);
     
     // Convert back to RGB
     lowp vec4    yIQ   = vec4 (YPrime, I, Q, 0.0);
     color.r = dot (yIQ, kYIQToR);
     color.g = dot (yIQ, kYIQToG);
     color.b = dot (yIQ, kYIQToB);
     
     return color;
 }
 
 mediump vec4 overlay(mediump vec4 base, mediump vec4 overlay) {
     mediump float ra;
     if (2.0 * base.r < base.a) {
         ra = 2.0 * overlay.r * base.r + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     } else {
         ra = overlay.a * base.a - 2.0 * (base.a - base.r) * (overlay.a - overlay.r) + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     }
     
     mediump float ga;
     if (2.0 * base.g < base.a) {
         ga = 2.0 * overlay.g * base.g + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     } else {
         ga = overlay.a * base.a - 2.0 * (base.a - base.g) * (overlay.a - overlay.g) + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     }
     
     mediump float ba;
     if (2.0 * base.b < base.a) {
         ba = 2.0 * overlay.b * base.b + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     } else {
         ba = overlay.a * base.a - 2.0 * (base.a - base.b) * (overlay.a - overlay.b) + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     }
     return vec4(ra, ga, ba, 1.0);
 }
 
 highp float lum(lowp vec3 c) {
     return dot(c, vec3(0.3, 0.59, 0.11));
 }
 
 lowp vec3 clipcolor(lowp vec3 c) {
     highp float l = lum(c);
     lowp float n = min(min(c.r, c.g), c.b);
     lowp float x = max(max(c.r, c.g), c.b);
     
     if (n < 0.0) {
         c.r = l + ((c.r - l) * l) / (l - n);
         c.g = l + ((c.g - l) * l) / (l - n);
         c.b = l + ((c.b - l) * l) / (l - n);
     }
     if (x > 1.0) {
         c.r = l + ((c.r - l) * (1.0 - l)) / (x - l);
         c.g = l + ((c.g - l) * (1.0 - l)) / (x - l);
         c.b = l + ((c.b - l) * (1.0 - l)) / (x - l);
     }
     
     return c;
 }
 
 lowp vec3 setlum(lowp vec3 c, highp float l) {
     highp float d = l - lum(c);
     c = c + vec3(d);
     return clipcolor(c);
 }
 
 highp float hash(highp float n )
{
    return fract(sin(n)*43758.5453123);
}
 
 highp float noise( highp vec2 x )
{
    highp vec2 p = floor(x);
    highp vec2 f = fract(x);
    
    f = f*f*(3.0-2.0*f);
    
    highp float n = p.x + p.y*157.0;
    
    return mix(mix( hash(n+  0.0), hash(n+  1.0),f.x),
               mix( hash(n+157.0), hash(n+158.0),f.x),f.y);
}
 
 void main() {
     
     lowp vec4 textureColorCenter = texture2D(inputImageTexture, textureCoordinate);
     
     lowp float maskY = 0.2989 * colorBlend.r + 0.5866 * colorBlend.g + 0.1145 * colorBlend.b;
     lowp float maskCr = 0.7132 * (colorBlend.r - maskY);
     lowp float maskCb = 0.5647 * (colorBlend.b - maskY);
     
     lowp float Y = 0.2989 * textureColorCenter.r + 0.5866 * textureColorCenter.g + 0.1145 * textureColorCenter.b;
     lowp float Cr = 0.7132 * (textureColorCenter.r - Y);
     lowp float Cb = 0.5647 * (textureColorCenter.b - Y);
     
     lowp float blendValue = 1.0 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), vec3(maskY,maskCr, maskCb)));
     
     if (blendValue > 0.4) {
         lowp vec4 textureColorLeft = texture2D(inputImageTexture, textureCoordinate-mod(vec2(noise(textureCoordinate+time),1.0), 0.0));
         lowp vec4 textureColorRight = texture2D(inputImageTexture, textureCoordinate+mod(vec2(noise(textureCoordinate+time),1.0) , 0.0));
         
         lowp vec4 c = hue(vec4(textureColorLeft.r, textureColorCenter.g, textureColorRight.b, textureColorCenter.a), time*1.5);
         
         if (blendType == 0) {
             c = overlay(c, textureColorCenter);
         }
         if (blendType == 1) {
             c = c * textureColorCenter + c * (1.0 - textureColorCenter.a) + textureColorCenter * (1.0 - c.a);
         }
         
         gl_FragColor = mix(textureColorCenter, c, blendValue);
     } else {
         gl_FragColor = textureColorCenter;
     }
 }
 
 );


@interface HueCycleFilter () {
    GLint colorBlendUniform;
    GLint timeUniform;
    GLint intensityUniform;
    GLint sensitivityUniform;
    GLint blendTypeUniform;
    GLint touchXUniform;
    GLint touchYUniform;
    GLint resXUniform;
    GLint resYUniform;
}

@end

@implementation HueCycleFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageHueCycleFragmentShaderString])) {
        return nil;
    }
    
    colorBlendUniform = [filterProgram uniformIndex:@"colorBlend"];
    timeUniform = [filterProgram uniformIndex:@"time"];
    intensityUniform = [filterProgram uniformIndex:@"intensity"];
    sensitivityUniform = [filterProgram uniformIndex:@"sensitivity"];
    blendTypeUniform = [filterProgram uniformIndex:@"blendType"];
    
    return self;
}

- (void)setTime:(float)t {
    _time = t;
    [self setFloat:t forUniform:timeUniform program:filterProgram];
}

- (void)setColorBlendValue:(UIColor *)colorBlendValue {
    _colorBlendValue = colorBlendValue;
    
    CGFloat r, g, b, a;
    [colorBlendValue getRed:&r green:&g blue:&b alpha:&a];
    
    GPUVector3 c;
    c.one = r;
    c.two = g;
    c.three = b;
    
    [self setVec3:c forUniform:colorBlendUniform program:filterProgram];
}

- (void)setIntensity:(float)intensity {
    _intensity = intensity;
    [self setFloat:intensity forUniform:intensity program:filterProgram];
}

- (void)setSensitivity:(float)sensitivity {
    _sensitivity = sensitivity;
    [self setFloat:_sensitivity forUniform:sensitivityUniform program:filterProgram];
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [self setInteger:_blendType forUniform:blendTypeUniform program:filterProgram];
}

@end
