//
//  WaterFilter.h
//  minitrip
//
//  Created by Connor Bell on 2015-04-20.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "GPUImageFilter.h"
#import "Effect.h"

@interface WaterFilter : GPUImageFilter

@property(readwrite, nonatomic) UIColor *colorBlendValue;
@property(readwrite, nonatomic) BlendType blendType;
@property(readwrite, nonatomic) float intensity;
@property(readwrite, nonatomic) float sensitivity;
@property(readwrite, nonatomic) float time;

@end
