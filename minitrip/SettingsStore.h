//
//  SettingsStore.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-23.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    VideoQualityMedium,
    VideoQualityHigh
} VideoQuality;

@interface SettingsStore : NSObject

+ (SettingsStore *)sharedInstance;

@property (nonatomic, readwrite) BOOL microphoneAudioEnabled;
@property (nonatomic, readwrite) VideoQuality videoQuality;
@property (nonatomic, readwrite) BOOL touchSamplesOriginalImage;

@end
