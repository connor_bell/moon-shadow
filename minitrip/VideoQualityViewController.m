//
//  VideoQualityViewController.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-25.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "VideoQualityViewController.h"
#import "SettingsStore.h"
#import "UIHelper.h"

@interface VideoQualityViewController ()

@end

@implementation VideoQualityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    VideoQuality quality = [[SettingsStore sharedInstance] videoQuality];
    
    if (indexPath.row == 0) {
        if (quality == VideoQualityHigh) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    if (indexPath.row == 1) {
        if (quality == VideoQualityMedium) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if ([UIHelper highQualityVideoSupported]) {
            [[SettingsStore sharedInstance] setVideoQuality:VideoQualityHigh];
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Performance Warning" message:@"Your device may not be able to have a decent framerate in high quality mode." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Continue", nil];
            [alertView show];
        }
    } else {
        [[SettingsStore sharedInstance] setVideoQuality:VideoQualityMedium];
    }
    [self.tableView reloadData];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.cancelButtonIndex != buttonIndex) {
        [[SettingsStore sharedInstance] setVideoQuality:VideoQualityHigh];
        [self.tableView reloadData];
    }
}

@end
