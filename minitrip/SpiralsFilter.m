//
//  BlixyFilter.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-23.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "SpiralsFilter.h"

NSString *const kGPUImageSpiralsFragmentShaderString = SHADER_STRING
(
 
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform lowp vec3 colorBlend;
 uniform highp float time;
 uniform lowp float sensitivity;
 uniform lowp float intensity;
 uniform lowp int blendType;
 uniform lowp float touchX;
 uniform lowp float touchY;
 
 mediump vec4 overlay(mediump vec4 base, mediump vec4 overlay) {
     mediump float ra;
     if (2.0 * base.r < base.a) {
         ra = 2.0 * overlay.r * base.r + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     } else {
         ra = overlay.a * base.a - 2.0 * (base.a - base.r) * (overlay.a - overlay.r) + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     }
     
     mediump float ga;
     if (2.0 * base.g < base.a) {
         ga = 2.0 * overlay.g * base.g + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     } else {
         ga = overlay.a * base.a - 2.0 * (base.a - base.g) * (overlay.a - overlay.g) + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     }
     
     mediump float ba;
     if (2.0 * base.b < base.a) {
         ba = 2.0 * overlay.b * base.b + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     } else {
         ba = overlay.a * base.a - 2.0 * (base.a - base.b) * (overlay.a - overlay.b) + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     }
     return vec4(ra, ga, ba, 1.0);
 }
 
 highp vec2 hash(highp vec2 v) {
     highp vec2 n;
     n.x=fract(fract(v.y-v.x*831.0518)*(v.y+v.x)*3156.7821);
     n.y=fract(fract(v.x+v.y*804.3048)*(v.x-v.y)*5349.2627);
     return n;
 }
 
 void main()
 {
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);

     lowp vec2 newPoint;
     lowp vec2 touch = vec2(touchX, touchY);
     lowp vec2 uv = textureCoordinate;

     // Convert to unit circle dealie
     lowp float aX = abs(uv.x - touch.x);
     lowp float aY = abs(uv.y - touch.y);
     
     lowp float speed = 0.5;
     lowp float intensity = 0.5;
     lowp float k = time * speed;
     
     lowp vec2 direction = -vec2(aX, aY);
     lowp float dist = distance(vec2(aX, aY), vec2(0,0)) * intensity;
     
     lowp vec2 newPos = vec2(aX, aY) + mod(k + dist, 1.0) * direction;
     
     newPoint = vec2((newPos.x + 1.0) / 2.0, (newPos.y + 1.0) / 2.0);
     
     lowp vec4 c = texture2D(inputImageTexture, newPoint);
     
     lowp float maskY = 0.2989 * colorBlend.r + 0.5866 * colorBlend.g + 0.1145 * colorBlend.b;
     lowp float maskCr = 0.7132 * (colorBlend.r - maskY);
     lowp float maskCb = 0.5647 * (colorBlend.b - maskY);
     
     lowp float Y = 0.2989 * textureColor.r + 0.5866 * textureColor.g + 0.1145 * textureColor.b;
     lowp float Cr = 0.7132 * (textureColor.r - Y);
     lowp float Cb = 0.5647 * (textureColor.b - Y);
     
     lowp float blendValue = 1.0 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), vec3(maskY,maskCr, maskCb)));

     if (blendType == 0) {
         c = overlay(textureColor, c);
     }
     if (blendType == 1) {
         c = c * textureColor + c * (1.0 - textureColor.a) + textureColor * (1.0 - c.a);
     }
     
     gl_FragColor = mix(textureColor, c, blendValue);
 }
 );

@interface SpiralsFilter () {
    GLint colorBlendUniform;
    GLint timeUniform;
    GLint intensityUniform;
    GLint sensitivityUniform;
    GLint blendTypeUniform;
    GLint touchXUniform;
    GLint touchYUniform;
}

@end

@implementation SpiralsFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageSpiralsFragmentShaderString])) {
        return nil;
    }
    
    colorBlendUniform = [filterProgram uniformIndex:@"colorBlend"];
    timeUniform = [filterProgram uniformIndex:@"time"];
    intensityUniform = [filterProgram uniformIndex:@"intensity"];
    sensitivityUniform = [filterProgram uniformIndex:@"sensitivity"];
    blendTypeUniform = [filterProgram uniformIndex:@"blendType"];
    touchXUniform = [filterProgram uniformIndex:@"touchX"];
    touchYUniform = [filterProgram uniformIndex:@"touchY"];
    
    return self;
}

- (void)setTime:(float)t {
    _time = t;
    [self setFloat:t forUniform:timeUniform program:filterProgram];
}

- (void)setColorBlendValue:(UIColor *)colorBlendValue {
    _colorBlendValue = colorBlendValue;
    
    CGFloat r, g, b, a;
    [colorBlendValue getRed:&r green:&g blue:&b alpha:&a];
    
    GPUVector3 c;
    c.one = r;
    c.two = g;
    c.three = b;
    
    [self setVec3:c forUniform:colorBlendUniform program:filterProgram];
}

- (void)setIntensity:(float)intensity {
    _intensity = intensity;
    [self setFloat:intensity forUniform:intensity program:filterProgram];
}

- (void)setSensitivity:(float)sensitivity {
    _sensitivity = sensitivity;
    [self setFloat:_sensitivity forUniform:sensitivityUniform program:filterProgram];
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [self setInteger:_blendType forUniform:blendTypeUniform program:filterProgram];
}

- (void)setTouchX:(float)touchX {
    _touchX = touchX;
    [self setFloat:_touchX forUniform:touchXUniform program:filterProgram];
}

- (void)setTouchY:(float)touchY {
    _touchY = touchY;
    [self setFloat:_touchY forUniform:touchYUniform program:filterProgram];
}

@end
