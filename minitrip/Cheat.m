//
//  Cheat.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-28.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "Cheat.h"

@implementation Cheat

+ (Cheat *)cheatWithTitle:(NSString *)title unlockCode:(NSString *)code enabledBlock:(CheatBlock)enabled disabledBlock:(CheatBlock)disabled {
    Cheat *c = [Cheat new];
    c.title = title;
    c.unlockCode = code;
    c.enabledBlock = enabled;
    c.disabledBlock = disabled;
    return c;
}

@end
