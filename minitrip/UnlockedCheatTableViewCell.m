//
//  UnlockedCheatTableViewCell.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-29.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "UnlockedCheatTableViewCell.h"
#import "CheatStore.h"

@implementation UnlockedCheatTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    return self;
}

- (NSString *)reuseIdentifier {
    return @"UnlockedCheatTableViewCell";
}

- (IBAction)unlockedSwitchTapped:(id)sender {
    if (_cheat.isEnabled) {
        _cheat.disabledBlock();
    } else {
        _cheat.enabledBlock();
    }
    _cheat.isEnabled = !_cheat.isEnabled;
    [[CheatStore sharedInstance] saveToDefaults];
}

@end
