//
//  CheatStore.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-28.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "CheatStore.h"
#import <UIKit/UIKit.h>

static CheatStore *instance = nil;
static NSString *CHEAT_CODES_KEY = @"CHEAT_CODES_KEY";
static NSString *CHEAT_CODES_ENABLED_KEY = @"CHEAT_CODES_ENABLED_KEY";

@interface CheatStore () {
    NSMutableArray *_allCheats;
    Cheat *_sunshine;
    Cheat *_glitchWizard;
    Cheat *_random;
    Cheat *_clear;
}

@end

@implementation CheatStore

+ (CheatStore *)sharedInstance {
    @synchronized(self) {
        if (!instance) {
            instance = [[self alloc] init];
        }
    }
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        _allCheats = [NSMutableArray array];
        [self loadCheats];
    }
    return self;
}

- (void)loadCheats {

    _sunshine = [Cheat cheatWithTitle:@"SUNSHINE GLITCH" unlockCode:@"sunshine" enabledBlock:^{
    
    } disabledBlock:^{
    
    }];
    
    _glitchWizard = [Cheat cheatWithTitle:@"GLITCHWIZ GLITCH" unlockCode:@"glitchwizard" enabledBlock:^{
        
    } disabledBlock:^{
        
    }];
    
    _random = [Cheat cheatWithTitle:@"RANDOM MODE" unlockCode:@"random" enabledBlock:^{
        
    } disabledBlock:^{
        
    }];

    _clear = [Cheat cheatWithTitle:@"SHAKE TO CLEAR" unlockCode:@"milkshake" enabledBlock:^{
        
    } disabledBlock:^{
        
    }];
    
    [_allCheats addObject:_glitchWizard];
    [_allCheats addObject:_random];
    [_allCheats addObject:_sunshine];
    [_allCheats addObject:_clear];
    [self loadUnlockedValuesFromDefaults];
}

- (void)saveToDefaults {
    // save the bool values to defaults
    NSMutableDictionary *dictionaryToSave = [NSMutableDictionary new];
    NSMutableDictionary *enabledDictionaryToSave = [NSMutableDictionary new];
    
    for (Cheat *cheat in _allCheats) {
        [dictionaryToSave setObject:@(cheat.isUnlocked) forKey:cheat.title];
        [enabledDictionaryToSave setObject:@(cheat.isEnabled) forKey:cheat.title];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[dictionaryToSave copy] forKey:CHEAT_CODES_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:[enabledDictionaryToSave copy] forKey:CHEAT_CODES_ENABLED_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// Run this after load cheats
- (void)loadUnlockedValuesFromDefaults {

    NSDictionary *vals = [[NSUserDefaults standardUserDefaults] objectForKey:CHEAT_CODES_KEY];
    NSDictionary *enabledVals = [[NSUserDefaults standardUserDefaults] objectForKey:CHEAT_CODES_ENABLED_KEY];

    if (vals) {
        for (Cheat *cheat in _allCheats) {
            if ([vals objectForKey:cheat.title] != nil && [enabledVals objectForKey:cheat.title]) {
                cheat.isUnlocked = [[vals objectForKey:cheat.title] boolValue];
                cheat.isEnabled = [[enabledVals objectForKey:cheat.title] boolValue];
                if (cheat.isEnabled) {
                    cheat.enabledBlock();
                }
            }
        }
    }
}

- (void)unlockCheat:(Cheat *)cheat {
    cheat.isUnlocked = YES;
    [self saveToDefaults];
}

- (NSArray *)getUnlockedCheats {
    return [_allCheats objectsAtIndexes:[_allCheats indexesOfObjectsPassingTest:^BOOL(Cheat *cheat, NSUInteger idx, BOOL *stop) {
        return cheat.isUnlocked;
    }]];
}

- (Cheat *)codeUnlocksCheat:(NSString *)code {
    NSInteger index = [_allCheats indexOfObjectPassingTest:^BOOL(Cheat *cheat, NSUInteger idx, BOOL *stop) {
        return [cheat.unlockCode isEqualToString:[code lowercaseString]];
    }];
    if (index == NSNotFound) return nil;
    Cheat *unlockedCheat = [_allCheats objectAtIndex:index];
    [self unlockCheat:unlockedCheat];
    return unlockedCheat;
}

- (BOOL)isSunshineGlitchEnabled {
    return _sunshine.isEnabled;
}

- (BOOL)isClearGlitchEnabled {
    return _clear.isEnabled;
}

- (BOOL)isClearGlitchUnlocked {
    return _clear.isUnlocked;
}

- (BOOL)isGlitchWizardGlitchEnabled {
    return _glitchWizard.isEnabled;
}

- (BOOL)isRandomGlitchEnabled {
    return _random.isEnabled;
}


@end
