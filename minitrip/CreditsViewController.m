//
//  CreditsViewController.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-05-06.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "CreditsViewController.h"
#import "SocialUtil.h"

@interface CreditsViewController () {
    NSDictionary *_twitterHandleDict;
}

@end

@implementation CreditsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_scrollView setContentSize:CGSizeMake(_scrollView.bounds.size.width, 1000)];

    _twitterHandleDict = @ { @(0) : @"macbooktall",
                             @(1) : @"netgrind",
                             @(2) : @"horsman",
                             @(3) : @"botreats",
                             @(4) : @"echophons",
                             @(5) : @"kaileymander",
                             @(6) : @"salemskates",
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)twitterButtonTapped:(id)sender {
    NSNumber *key = @([(UIButton*)sender tag]);
    NSString *handle = _twitterHandleDict[key];
    [SocialUtil openUserTwitter:handle];
}

- (IBAction)glitchWizardTapped:(id)sender {
    NSString *iTunesLink = @"itms://itunes.apple.com/ca/app/glitch-wizard-databent-gif/id904640439?mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 7) {
        [self performSegueWithIdentifier:@"SEGUE_ID_GPUIMAGE" sender:nil];
    }
}

@end
