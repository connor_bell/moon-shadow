//
//  InfoView.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-27.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    InfoViewEffectSelect,
    InfoViewImageTapped,
    InfoViewSensitivityTapped
} InfoViewAction;

@interface InfoView : UIView

@property (nonatomic, strong) IBOutlet UILabel *infoLabel;
@property (nonatomic, strong) IBOutlet UIImageView *arrowImageView;
@property (nonatomic, strong) IBOutlet UIButton *shareButton;
@property (nonatomic, strong) IBOutlet UIButton *previewButton;
@property (nonatomic, readonly) BOOL latestSaveVideo;

- (void)showIfFirstLaunch;
- (void)action:(InfoViewAction)act;
- (void)showImageSaved;
- (void)showVideoSaved;
- (void)showError:(NSString *)errorMessage;

@end
