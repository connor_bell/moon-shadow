//
//  AppDelegate.h
//  minitrip
//
//  Created by Connor Bell on 2014-12-25.
//  Copyright (c) 2014 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

