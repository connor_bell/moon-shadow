#import "HotPavementEffect.h"
#import "HotPavementFilter.h"

@interface HotPavementEffect () {
    HotPavementFilter *_imageFilter;
}
@end

@implementation HotPavementEffect
@synthesize currentColor=_currentColor;
@synthesize blendType=_blendType;

- (instancetype)init {
    if (self = [super init]) {
        _imageFilter = [HotPavementFilter new];
        [_imageFilter setSensitivity:EFFECT_START_VAL];
        self.blendType = BlendTypeReplace;
    }
    return self;
}

- (NSString *)title {
    return @"HOTPVMNT";
}

- (UIImage *)previewImage {
    return [UIImage imageNamed:@"HOTPVMNT"];
}

- (void)updateTimewithValue:(float)value {
    [_imageFilter setTime:value];
}

- (void)updateSensitivityWithValue:(float)value {
    [_imageFilter setSensitivity:value];
}

- (void)setCurrentColor:(UIColor *)currentColor {
    _currentColor = currentColor;
    [_imageFilter setColorBlendValue:_currentColor];
}

- (GPUImageFilter *)imageFilter {
    return _imageFilter;
}

- (float)getIntensity {
    return _imageFilter.sensitivity;
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [_imageFilter setBlendType:blendType];
}

@end
