#import "FalseGodFilter.h"

NSString *const kGPUImageFalseGodFragmentShaderString = SHADER_STRING
(
 
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform lowp vec3 colorBlend;
 uniform highp float time;
 uniform lowp float sensitivity;
 uniform lowp float intensity;
 uniform lowp int blendType;
 uniform lowp vec3 mask;

 highp float lum(lowp vec3 c) {
     return dot(c, vec3(0.3, 0.59, 0.11));
 }
 
 lowp vec3 clipcolor(lowp vec3 c) {
     highp float l = lum(c);
     lowp float n = min(min(c.r, c.g), c.b);
     lowp float x = max(max(c.r, c.g), c.b);
     
     if (n < 0.0) {
         c.r = l + ((c.r - l) * l) / (l - n);
         c.g = l + ((c.g - l) * l) / (l - n);
         c.b = l + ((c.b - l) * l) / (l - n);
     }
     if (x > 1.0) {
         c.r = l + ((c.r - l) * (1.0 - l)) / (x - l);
         c.g = l + ((c.g - l) * (1.0 - l)) / (x - l);
         c.b = l + ((c.b - l) * (1.0 - l)) / (x - l);
     }
     
     return c;
 }
 
 lowp vec3 setlum(lowp vec3 c, highp float l) {
     highp float d = l - lum(c);
     c = c + vec3(d);
     return clipcolor(c);
 }
 
 highp vec3 mod289(highp vec3 x) {
     return x - floor(x * (1.0 / 289.0)) * 289.0;
 }
 
 highp vec4 mod289(highp vec4 x) {
     return x - floor(x * (1.0 / 289.0)) * 289.0;
 }
 
 highp vec4 permute(highp vec4 x) {
     return mod289(((x*34.0)+1.0)*x);
 }
 
 highp vec4 taylorInvSqrt(highp vec4 r){
     return 1.79284291400159 - 0.85373472095314 * r;
 }
 
 lowp float snoise(highp vec3 v){
     const lowp vec2  C = vec2(1.0/6.0, 1.0/3.0) ;
     const lowp vec4  D = vec4(0.0, 0.5, 1.0, 2.0);
     
     // First corner
     highp vec3 i  = floor(v + dot(v, C.yyy) );
     highp vec3 x0 =   v - i + dot(i, C.xxx) ;
     
     // Other corners
     highp vec3 g = step(x0.yzx, x0.xyz);
     highp vec3 l = 1.0 - g;
     highp vec3 i1 = min( g.xyz, l.zxy );
     highp vec3 i2 = max( g.xyz, l.zxy );
     
     //   x0 = x0 - 0.0 + 0.0 * C.xxx;
     //   x1 = x0 - i1  + 1.0 * C.xxx;
     //   x2 = x0 - i2  + 2.0 * C.xxx;
     //   x3 = x0 - 1.0 + 3.0 * C.xxx;
     highp vec3 x1 = x0 - i1 + C.xxx;
     highp vec3 x2 = x0 - i2 + C.yyy; // 2.0*C.x = 1/3 = C.y
     highp vec3 x3 = x0 - D.yyy;      // -1.0+3.0*C.x = -0.5 = -D.y
     
     // Permutations
     i = mod289(i);
     highp vec4 p = permute( permute( permute(
                                              i.z + vec4(0.0, i1.z, i2.z, 1.0 ))
                                     + i.y + vec4(0.0, i1.y, i2.y, 1.0 ))
                            + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));
     
     // Gradients: 7x7 points over a square, mapped onto an octahedron.
     // The ring size 17*17 = 289 is close to a multiple of 49 (49*6 = 294)
     lowp float n_ = 0.142857142857; // 1.0/7.0
     lowp vec3  ns = n_ * D.wyz - D.xzx;
     
     lowp vec4 j = p - 49.0 * floor(p * ns.z * ns.z);  //  mod(p,7*7)
     
     lowp vec4 x_ = floor(j * ns.z);
     lowp vec4 y_ = floor(j - 7.0 * x_ );    // mod(j,N)
     
     lowp vec4 x = x_ *ns.x + ns.yyyy;
     lowp vec4 y = y_ *ns.x + ns.yyyy;
     lowp vec4 h = 1.0 - abs(x) - abs(y);
     
     lowp vec4 b0 = vec4( x.xy, y.xy );
     lowp vec4 b1 = vec4( x.zw, y.zw );
     
     //vec4 s0 = vec4(lessThan(b0,0.0))*2.0 - 1.0;
     //vec4 s1 = vec4(lessThan(b1,0.0))*2.0 - 1.0;
     lowp vec4 s0 = floor(b0)*2.0 + 1.0;
     lowp vec4 s1 = floor(b1)*2.0 + 1.0;
     lowp vec4 sh = -step(h, vec4(0.0));
     
     lowp vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
     lowp vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;
     
     lowp vec3 p0 = vec3(a0.xy,h.x);
     lowp vec3 p1 = vec3(a0.zw,h.y);
     lowp vec3 p2 = vec3(a1.xy,h.z);
     lowp vec3 p3 = vec3(a1.zw,h.w);
     
     //Normalise gradients
     highp vec4 norm = taylorInvSqrt(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
     p0 *= norm.x;
     p1 *= norm.y;
     p2 *= norm.z;
     p3 *= norm.w;
     
     // Mix final noise value
     lowp vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
     m = m * m;
     return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1),
                                  dot(p2,x2), dot(p3,x3) ) );
 }
 
 lowp vec4 hue(lowp vec4 color, lowp float shift, lowp float chroma) {
     
     lowp  vec4  kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
     lowp  vec4  kRGBToI     = vec4 (0.596, -0.275, -0.321, 0.0);
     lowp  vec4  kRGBToQ     = vec4 (0.212, -0.523, 0.311, 0.0);
     
     lowp  vec4  kYIQToR   = vec4 (1.0, 0.956, 0.621, 0.0);
     lowp  vec4  kYIQToG   = vec4 (1.0, -0.272, -0.647, 0.0);
     lowp  vec4  kYIQToB   = vec4 (1.0, -1.107, 1.704, 0.0);
     
     // Convert to YIQ
     lowp float   YPrime  = dot (color, kRGBToYPrime);
     lowp float   I      = dot (color, kRGBToI);
     lowp float   Q      = dot (color, kRGBToQ);
     
     // Calculate the hue and chroma
     lowp float   hue     = atan (Q, I);
     
     // Make the user's adjustments
     hue += shift;
     
     // Convert back to YIQ
     Q = chroma * sin (hue);
     I = chroma * cos (hue);
     
     // Convert back to RGB
     lowp vec4    yIQ   = vec4 (YPrime, I, Q, 0.0);
     color.r = dot (yIQ, kYIQToR);
     color.g = dot (yIQ, kYIQToG);
     color.b = dot (yIQ, kYIQToB);
     
     return color;
 }
 
 highp float turbulence(lowp vec3 p, lowp float lacunarity, lowp float gain) {
     highp float sum = 0.0;
     highp float freq = 1.0;
     highp float amp = 1.0;
     for(int i = 0; i < 3; i++){
         sum += snoise(p*freq)*amp;
         freq = freq * lacunarity;
         amp = amp * gain;
     }
     return (sum + 1.0)*0.5 ;
 }
 
 void main()
{
    lowp vec4 textureColor = texture2D(inputImageTexture,textureCoordinate);
    
    lowp float Y = 0.2989 * textureColor.r + 0.5866 * textureColor.g + 0.1145 * textureColor.b;
    lowp float Cr = 0.7132 * (textureColor.r - Y);
    lowp float Cb = 0.5647 * (textureColor.b - Y);
    
    lowp float blendValue = 1.0 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), mask));
    
    if (blendValue > 0.25) {
        highp float angle = turbulence(vec3(textureCoordinate,time*0.1), 1.8,0.8)*0.6+2.6;
        highp vec4 c = hue(texture2D(inputImageTexture,textureCoordinate),angle, 0.4*textureCoordinate.y+0.2);
        
        if (blendType == 0) {
            gl_FragColor = mix(textureColor, c, blendValue);
        } else if (blendType == 1) {
            c = c * textureColor + c * (1.0 - textureColor.a) + textureColor * (1.0 - c.a);
            gl_FragColor = mix(textureColor, c, blendValue);
        } else {
            gl_FragColor = c;
        }

    } else {
        gl_FragColor = textureColor;
    }
}
 
 );

@interface FalseGodFilter () {
    GLint colorBlendUniform;
    GLint timeUniform;
    GLint intensityUniform;
    GLint sensitivityUniform;
    GLint blendTypeUniform;
    GLint maskUniform;
}

@end

@implementation FalseGodFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageFalseGodFragmentShaderString])) {
        return nil;
    }
    
    colorBlendUniform = [filterProgram uniformIndex:@"colorBlend"];
    timeUniform = [filterProgram uniformIndex:@"time"];
    intensityUniform = [filterProgram uniformIndex:@"intensity"];
    sensitivityUniform = [filterProgram uniformIndex:@"sensitivity"];
    blendTypeUniform = [filterProgram uniformIndex:@"blendType"];
    maskUniform = [filterProgram uniformIndex:@"mask"];

    return self;
}

- (void)setTime:(float)t {
    _time = t;
    [self setFloat:t forUniform:timeUniform program:filterProgram];
}

- (void)setColorBlendValue:(UIColor *)colorBlendValue {
    _colorBlendValue = colorBlendValue;
    
    CGFloat r, g, b, a;
    [colorBlendValue getRed:&r green:&g blue:&b alpha:&a];
    
    GPUVector3 c;
    c.one = r;
    c.two = g;
    c.three = b;
    
    GPUVector3 mask;
    mask.one = 0.2989 * r + 0.5866 * g + 0.1145 * b;
    mask.two = 0.7132 * (r - mask.one);
    mask.three = 0.5647 * (b - mask.one);
    
    [self setVec3:mask forUniform:maskUniform program:filterProgram];
    [self setVec3:c forUniform:colorBlendUniform program:filterProgram];
}

- (void)setIntensity:(float)intensity {
    _intensity = intensity;
    [self setFloat:intensity forUniform:intensity program:filterProgram];
}

- (void)setSensitivity:(float)sensitivity {
    _sensitivity = sensitivity;
    [self setFloat:_sensitivity forUniform:sensitivityUniform program:filterProgram];
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [self setInteger:_blendType forUniform:blendTypeUniform program:filterProgram];
}

@end
