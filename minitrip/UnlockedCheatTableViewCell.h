//
//  UnlockedCheatTableViewCell.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-29.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cheat.h"

@interface UnlockedCheatTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UISwitch *unlockedSwitch;
@property (nonatomic, assign) Cheat *cheat;

- (IBAction)unlockedSwitchTapped:(id)sender;

@end
