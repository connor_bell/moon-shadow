//
//  VideoQualityViewController.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-25.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoQualityViewController : UITableViewController <UIAlertViewDelegate>

@end
