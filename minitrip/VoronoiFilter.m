//
//  VoronoiFilter.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-23.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "VoronoiFilter.h"
#import <CoreMotion/CoreMotion.h>

NSString *const kGPUImageVoronoiFragmentShaderString = SHADER_STRING
(
 
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform lowp vec3 colorBlend;
 uniform highp float time;
 uniform lowp float sensitivity;
 uniform lowp float intensity;
 uniform lowp int blendType;
 uniform lowp float xAxisAccel;
 uniform lowp float touchPosX;
 uniform lowp float touchPosY;
 uniform highp float resX;
 uniform highp float resY;
 
lowp vec4 hue(lowp vec4 color, highp float shift) {
     
     const lowp vec4 kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
     const lowp vec4 kRGBToI     = vec4 (0.596, -0.275, -0.321, 0.0);
     const lowp vec4 kRGBToQ     = vec4 (0.212, -0.523, 0.311, 0.0);
     
     const lowp vec4 kYIQToR   = vec4 (1.0, 0.956, 0.621, 0.0);
     const lowp vec4 kYIQToG   = vec4 (1.0, -0.272, -0.647, 0.0);
     const lowp vec4 kYIQToB   = vec4 (1.0, -1.107, 1.704, 0.0);
     
     // Convert to YIQ
     lowp float   YPrime  = dot (color, kRGBToYPrime);
     lowp float   I      = dot (color, kRGBToI);
     lowp float   Q      = dot (color, kRGBToQ);
     
     // Calculate the hue and chroma
     lowp float   hue     = atan (Q, I);
     lowp float   chroma  = sqrt (I * I + Q * Q);
     
     // Make the user's adjustments
     hue += shift;
     
     // Convert back to YIQ
     Q = chroma * sin (hue);
     I = chroma * cos (hue);
     
     // Convert back to RGB
     lowp vec4    yIQ   = vec4 (YPrime, I, Q, 0.0);
     color.r = dot (yIQ, kYIQToR);
     color.g = dot (yIQ, kYIQToG);
     color.b = dot (yIQ, kYIQToB);
     
     return color;
 }
 
 highp float lum(lowp vec3 c) {
     return dot(c, vec3(0.3, 0.59, 0.11));
 }
 
 lowp vec3 clipcolor(lowp vec3 c) {
     highp float l = lum(c);
     lowp float n = min(min(c.r, c.g), c.b);
     lowp float x = max(max(c.r, c.g), c.b);
     
     if (n < 0.0) {
         c.r = l + ((c.r - l) * l) / (l - n);
         c.g = l + ((c.g - l) * l) / (l - n);
         c.b = l + ((c.b - l) * l) / (l - n);
     }
     if (x > 1.0) {
         c.r = l + ((c.r - l) * (1.0 - l)) / (x - l);
         c.g = l + ((c.g - l) * (1.0 - l)) / (x - l);
         c.b = l + ((c.b - l) * (1.0 - l)) / (x - l);
     }
     
     return c;
 }
 
 lowp vec3 setlum(lowp vec3 c, highp float l) {
     highp float d = l - lum(c);
     c = c + vec3(d);
     return clipcolor(c);
 }

 lowp float ease(lowp float t) {
     lowp float p = 2.0 * t * t;
     return t < 0.5 ? p : -p + (4.0 * t) - 1.0;
 }

 void main()
 {
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);

     highp vec2 iResolution = vec2(resX,resY);
     highp vec2 fragCoord = textureCoordinate * iResolution.xy;
     
     highp float i = time;
     highp vec2 uv = fragCoord.xy-iResolution.xy*.5;
     uv = uv / iResolution.xx*(3.0+(sin(i*0.2)));
     
//     //CONNOR - replace mouse with touch position going from -1 to +1
     highp vec2 touch = vec2(touchPosX, touchPosY);
     highp float d = distance(uv,touch)*sin(i);
     
//     //CONNOR - replace mouse with whatever accel angle feels best
     //lowp float a =  iMouse.x/iResolution.x*2.0-1.0;
     highp float a =  touch.x/iResolution.x*2.0-1.0;

     uv*= mat2(d,a,-a,d);
     
     lowp float j = mod(i,3.5);
     
     highp float f = (abs(mod(uv.x,1.0)-.5)-.45)*20.;
     f = max(f, (abs(mod(uv.y,0.5)-.25)-.2)*20.);
     
     f = max(f, (abs(mod(uv.y+uv.x*1.5,1.0)-.5)-.4)*10.);
     f = max(f, (abs(mod(uv.y+uv.x*-1.5,1.0)-.5)-.4)*10.);
     
     f = max(f, (abs(mod(uv.y+uv.x*0.5,1.0)-.5)-.45)*20.);
     f = max(f, (abs(mod(uv.y+uv.x*-0.5,1.0)-.5)-.45)*20.);
     
     lowp vec4 c = vec4(0.0,0.0,0.0,1.0);
     c.r = f;
     c.b = cos(f+sin(i))*.5+.5;
     c.g = abs(f);
     c = hue(c,i+d*0.3);
     
     lowp float maskY = 0.2989 * colorBlend.r + 0.5866 * colorBlend.g + 0.1145 * colorBlend.b;
     lowp float maskCr = 0.7132 * (colorBlend.r - maskY);
     lowp float maskCb = 0.5647 * (colorBlend.b - maskY);
     
     lowp float Y = 0.2989 * textureColor.r + 0.5866 * textureColor.g + 0.1145 * textureColor.b;
     lowp float Cr = 0.7132 * (textureColor.r - Y);
     lowp float Cb = 0.5647 * (textureColor.b - Y);
     
     lowp float blendValue = 1.0 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), vec3(maskY,maskCr, maskCb)));

     if (blendType == 0) {
         c = vec4(textureColor.rgb * (1.0 - c.a) + setlum(c.rgb, lum(textureColor.rgb)) * c.a, textureColor.a);
     } else if (blendType == 1) {
         c = c * textureColor + c * (1.0 - textureColor.a) + textureColor * (1.0 - c.a);
     } else {
         
     }
     
     gl_FragColor = mix(textureColor, c, blendValue);
 }
 );

@interface VoronoiFilter () {
    GLint colorBlendUniform;
    GLint timeUniform;
    GLint intensityUniform;
    GLint sensitivityUniform;
    GLint blendTypeUniform;
    GLint xAxisAccelUniform;
    GLint touchPosXUniform;
    GLint touchPosYUniform;
    float x;
    GLint resXUniform;
    GLint resYUniform;
}

@property (nonatomic, strong) CMMotionManager *motionManager;

@end

@implementation VoronoiFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageVoronoiFragmentShaderString])) {
        return nil;
    }
    
    colorBlendUniform = [filterProgram uniformIndex:@"colorBlend"];
    timeUniform = [filterProgram uniformIndex:@"time"];
    intensityUniform = [filterProgram uniformIndex:@"intensity"];
    sensitivityUniform = [filterProgram uniformIndex:@"sensitivity"];
    blendTypeUniform = [filterProgram uniformIndex:@"blendType"];
    xAxisAccelUniform = [filterProgram uniformIndex:@"xAxisAccel"];
    touchPosXUniform = [filterProgram uniformIndex:@"touchPosX"];
    touchPosYUniform = [filterProgram uniformIndex:@"touchPosY"];
    resXUniform = [filterProgram uniformIndex:@"resX"];
    resYUniform = [filterProgram uniformIndex:@"resY"];
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    [self setFloat:(float)bounds.size.width forUniform:resXUniform program:filterProgram];
    [self setFloat:(float)bounds.size.height forUniform:resYUniform program:filterProgram];
    
//    self.motionManager = [[CMMotionManager alloc] init];
//    self.motionManager.accelerometerUpdateInterval = 1;
//    
//    if ([self.motionManager isGyroAvailable]) {
//        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//        [self.motionManager startGyroUpdatesToQueue:queue withHandler:^(CMGyroData *gyroData, NSError *error) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //[self setFloat:gyroData.rotationRate.y forUniform:xAxisAccelUniform program:filterProgram];
//            });
//        }];
//    }
    
    return self;
}

- (void)setTime:(float)t {
    _time = t;
    [self setFloat:t forUniform:timeUniform program:filterProgram];
}

- (void)setColorBlendValue:(UIColor *)colorBlendValue {
    _colorBlendValue = colorBlendValue;
    
    CGFloat r, g, b, a;
    [colorBlendValue getRed:&r green:&g blue:&b alpha:&a];
    
    GPUVector3 c;
    c.one = r;
    c.two = g;
    c.three = b;
    
    [self setVec3:c forUniform:colorBlendUniform program:filterProgram];
}

- (void)setIntensity:(float)intensity {
    _intensity = intensity;
    [self setFloat:intensity forUniform:intensity program:filterProgram];
}

- (void)setSensitivity:(float)sensitivity {
    _sensitivity = sensitivity;
    [self setFloat:_sensitivity forUniform:sensitivityUniform program:filterProgram];
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [self setInteger:_blendType forUniform:blendTypeUniform program:filterProgram];
}

- (void)setTouchPos:(CGPoint)touchPos {
    _touchPos = touchPos;
    [self setFloat:_touchPos.x*2.0-1.0 forUniform:touchPosXUniform program:filterProgram];
    [self setFloat:_touchPos.y*2.0-1.0 forUniform:touchPosYUniform program:filterProgram];
}

@end
