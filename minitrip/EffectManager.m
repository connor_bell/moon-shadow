//
//  EffectManager.m
//  minitrip
//
//  Created by Connor Bell on 2015-04-12.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "EffectManager.h"
#import "Effect.h"
#import "TyeDyeEffect.h"
#import "SpaceEffect.h"
#import "HueyEffect.h"
#import "WaterEffect.h"
#import "MindTilesEffect.h"
#import "VoidNoiseEffect.h"
#import "BlixyEffect.h"
#import "SpiralsEffect.h"
#import "Spirals2Effect.h"
#import "ColorSymEffect.h"
#import "HotPavementEffect.h"
#import "FalseGodEffect.h"
#import "SunshineEffect.h"
#import "CheatStore.h"
#import "HueCycleEffect.h"
#import "RippleEffect.h"
#import "HexatilesEffect.h"
#import "HueRGBCycleEffect.h"
#import "ToonEffect.h"

static EffectManager *manager = nil;

@interface EffectManager () {
    Effect *_sunshineEffect;
    Effect *_hueCycleEffect;
}

@end

@implementation EffectManager
@synthesize effects=_effects;

+ (EffectManager *)sharedInstance {
    @synchronized(self) {
        if (!manager) {
            manager = [EffectManager new];
        }
    }
    return manager;
}

- (instancetype)init {
    if (!self) {
        self = [super init];
    }
    return self;
}

- (NSArray *)effects {
    if (!_effects) {
        Effect *tripEffect = [TyeDyeEffect new];
        Effect *spaceEffect = [SpaceEffect new];
        Effect *hueyEffect = [HueyEffect new];
        Effect *waterEffect = [WaterEffect new];
        Effect *mindTilesEffect = [MindTilesEffect new];
        Effect *voidNoiseEffect = [VoidNoiseEffect new];
        Effect *blixyEffect = [BlixyEffect new];
        Effect *spiralsEffect = [SpiralsEffect new];
        Effect *colorSymEffect = [ColorSymEffect new];
        Effect *spirals2Effect = [Spirals2Effect new];
        Effect *hotPavementEffect = [HotPavementEffect new];
        Effect *falseGodEffect = [FalseGodEffect new];
        Effect *rippleEffect = [RippleEffect new];
        Effect *hexatilesEffect = [HexatilesEffect new];
        Effect *rgbHueEffect = [HueRGBCycleEffect new];
        Effect *toonEffect = [ToonEffect new];
        
        _hueCycleEffect = [HueCycleEffect new];
        _sunshineEffect = [SunshineEffect new];
        
        _effects = [@[rgbHueEffect, tripEffect, spaceEffect, hueyEffect, waterEffect, mindTilesEffect, voidNoiseEffect, blixyEffect, rippleEffect, hexatilesEffect, spiralsEffect, colorSymEffect, spirals2Effect, hotPavementEffect, falseGodEffect, toonEffect, _sunshineEffect, _hueCycleEffect] mutableCopy];
    }
    
    return _effects;
}

- (void)modifyEffectsForActiveCheats {
    if ([[CheatStore sharedInstance] isSunshineGlitchEnabled]) {
        if (![_effects containsObject:_sunshineEffect]) {
            [_effects addObject:_sunshineEffect];
        }
    } else {
        if ([_effects containsObject:_sunshineEffect]) {
            [_effects removeObject:_sunshineEffect];
        }
    }
    
    if ([[CheatStore sharedInstance] isGlitchWizardGlitchEnabled]) {
        if (![_effects containsObject:_hueCycleEffect]) {
            [_effects addObject:_hueCycleEffect];
        }
    } else {
        if ([_effects containsObject:_hueCycleEffect]) {
            [_effects removeObject:_hueCycleEffect];
        }
    }
}

@end
