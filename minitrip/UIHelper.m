#import "UIHelper.h"
#import <sys/utsname.h>

NSString *NOTIFICATION_BACKGROUND_ENTER = @"NOTIFICATION_BACKGROUND_ENTER";
NSString *NOTIFICATION_BACKGROUND_EXIT = @"NOTIFICATION_BACKGROUND_EXIT";

@implementation UIHelper

+ (UIColor *)opaqueBackgroundColor {
    return [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
}

+ (UIColor *)transparentBackgroundColor {
    return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.25];
}

+ (UIImage*)rotateUIImage:(UIImage*)sourceImage dir:(UIDeviceOrientation)devOrientation {
    if (devOrientation == UIDeviceOrientationPortrait || devOrientation == UIDeviceOrientationPortraitUpsideDown) return sourceImage;
    
    CGSize size = sourceImage.size;
    
    if (devOrientation == UIDeviceOrientationLandscapeLeft || devOrientation == UIDeviceOrientationLandscapeRight) {
        size = CGSizeMake(size.height, size.width);
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(size.height, size.width));
    UIImageOrientation orientation = devOrientation == UIDeviceOrientationLandscapeLeft ? UIImageOrientationLeft : UIImageOrientationRight;
    
    [[UIImage imageWithCGImage:[sourceImage CGImage] scale:1.0 orientation:orientation] drawInRect:CGRectMake(0,0,size.height ,size.width)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIColor *)interpolate:(UIColor *)c1 c2:(UIColor *)c2 t:(float)t {
    CGFloat r1 = 0.0f, g1 = 0.0f, b1 = 0.0f;
    CGFloat r2 = 0.0f, g2 = 0.0f, b2 = 0.0f;
    [c1 getRed:&r1 green:&g1 blue:&b1 alpha:nil];
    [c2 getRed:&r2 green:&g2 blue:&b2 alpha:nil];
    
    return [UIColor colorWithRed:((1.0-t)*r1+t*r2) green:((1.0-t)*g1+t*g2) blue:((1.0-t)*b1+t*b2) alpha:1.0];
}

+ (BOOL)highQualityVideoSupported {
    struct utsname systemInfo;
    uname(&systemInfo);

    NSArray *highQualityEnabledDevices = @[@"iPhone6,1",
                                           @"iPhone6,2",
                                           @"iPhone7,1",
                                           @"iPhone7,2",
                                           @"iPad4,1",
                                           @"iPad4,2",
                                           @"iPad4,3",
                                           @"iPad4,4",
                                           @"iPad4,5",
                                           @"iPad4,6",
                                           @"iPad4,7",
                                           @"iPad4,8",
                                           @"iPad4,9",
                                           @"iPad5,3",
                                           @"iPad5,4",
                                           @"iPad3,1",
                                           @"iPad3,2",
                                           @"iPad3,3",
                                           @"iPad3,4",
                                           @"iPad3,5",
                                           @"iPad3,6"];
    
    NSString *name = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    NSInteger idx =  [highQualityEnabledDevices indexOfObjectPassingTest:^BOOL(NSString *obj, NSUInteger idx, BOOL *stop) {
        return [obj isEqualToString:name];
    }];
    
    return idx != NSNotFound;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height {
    CGFloat oldWidth = image.size.width;
    CGFloat oldHeight = image.size.height;
    
    CGFloat scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
    
    CGFloat newHeight = oldHeight * scaleFactor;
    CGFloat newWidth = oldWidth * scaleFactor;
    CGSize newSize = CGSizeMake(newWidth, newHeight);
    
    return [self imageWithImage:image scaledToSize:newSize];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
