//
//  WaterEffect.m
//  minitrip
//
//  Created by Connor Bell on 2015-04-20.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "WaterEffect.h"
#import "WaterFilter.h"

@interface WaterEffect () {
    WaterFilter *_imageFilter;
}

@end

@implementation WaterEffect
@synthesize currentColor=_currentColor;
@synthesize blendType=_blendType;

- (instancetype)init {
    if (self = [super init]) {
        _imageFilter = [WaterFilter new];
        [_imageFilter setSensitivity:EFFECT_START_VAL];
    }
    return self;
}

- (NSString *)title {
    return @"GLITCH";
}

- (UIImage *)previewImage {
    return [UIImage imageNamed:@"GLITCH"];
}

- (void)updateTimewithValue:(float)value {
    [_imageFilter setTime:value];
}

- (void)updateSensitivityWithValue:(float)value {
    [_imageFilter setSensitivity:value];
}

- (void)setCurrentColor:(UIColor *)currentColor {
    _currentColor = currentColor;
    [_imageFilter setColorBlendValue:_currentColor];
}

- (GPUImageFilter *)imageFilter {
    return _imageFilter;
}

- (float)getIntensity {
    return _imageFilter.sensitivity;
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [_imageFilter setBlendType:blendType];
}

@end
