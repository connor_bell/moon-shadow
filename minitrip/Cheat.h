//
//  Cheat.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-28.
//  Copyright (c) 2015 Connor Bell. All rights SNEAKY SNEAKY SNEAKY SNEAKY SNEAKY SNEAKY SNEAKY SNEAKY SNEAKY
//
// sneaky snakes podcast inc

#import <Foundation/Foundation.h>

typedef void (^CheatBlock)();

@interface Cheat : NSObject

@property (nonatomic, readwrite) BOOL isUnlocked;
@property (nonatomic, readwrite) BOOL isEnabled;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *unlockCode;
@property (readwrite, copy) CheatBlock enabledBlock;
@property (readwrite, copy) CheatBlock disabledBlock;

+ (Cheat *)cheatWithTitle:(NSString *)title unlockCode:(NSString *)code enabledBlock:(CheatBlock)enabled disabledBlock:(CheatBlock)disabled;

@end
