//
//  ViewController.h
//  minitrip
//
//  Created by Connor Bell on 2014-12-25.
//  Copyright (c) 2014 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GPUImage.h"
#import "LoadingOverlayView.h"
#import "InfoView.h"

typedef enum {
    VideoMode,
    ImageMode,
    ImportMediaMode,
} MoonShadowMode;

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate, GPUImageMovieDelegate>

@property (nonatomic, strong) GPUImageStillCamera *stillCamera;
@property (nonatomic, strong) IBOutlet UICollectionView *effectCollectionView;
@property (nonatomic, strong) IBOutlet GPUImageView *imageFilterView;

@property (nonatomic, strong) IBOutlet UIButton *captureButton;
@property (nonatomic, strong) IBOutlet UIButton *videoModeButton;
@property (nonatomic, strong) IBOutlet UIButton *switchCameraButton;
@property (nonatomic, strong) IBOutlet UIButton *settingsButton;
@property (nonatomic, strong) IBOutlet UIImageView *captureModeImage;
@property (nonatomic, strong) IBOutlet UILabel *videoTimerLabel;
@property (nonatomic, strong) IBOutlet UILabel *captureModeLabel;

@property (nonatomic, strong) IBOutlet UISlider *intensitySlider;
@property (nonatomic, strong) IBOutlet UIView *controlsView;
@property (nonatomic, strong) IBOutlet UIView *stillImageExportView;
@property (nonatomic, strong) IBOutlet UIView *bottomContainerView;

@property (nonatomic, strong) IBOutlet UIView *videoModeView;
@property (nonatomic, strong) IBOutlet UIView *colorView;
@property (nonatomic, strong) IBOutlet LoadingOverlayView *loadingOverlay;
@property (nonatomic, strong) IBOutlet InfoView *infoView;
@property (nonatomic, strong) IBOutlet UIButton *randomButton;

@property (nonatomic, strong) IBOutlet UIView *previewControlsContainerView;
@property (nonatomic, strong) IBOutlet UISlider *timeControlSlider;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint *controlsViewYPosConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *imageViewYPosConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *infoViewYPosConstraint;
@property (nonatomic, strong) IBOutlet UISegmentedControl *blendModeControl;

- (IBAction)captureButtonTapped;
- (IBAction)saveImageTapped:(id)sender;
- (IBAction)cancelSaveImageTapped:(id)sender;
- (IBAction)previewSliderChanged:(id)sender;
- (IBAction)alphaSliderChanged:(id)sender;
- (IBAction)saveStillImageTapped:(id)sender;
- (IBAction)cancelStillImageTapped:(id)sender;
- (IBAction)switchCameraTapped:(id)sender;
- (IBAction)toggleControlsViewTapped:(id)sender;
- (IBAction)toggleVideoModeTapped:(id)sender;
- (IBAction)settingsButtonTapped:(id)sender;
- (IBAction)blendModeChanged:(id)sender;
- (IBAction)shareTapped:(id)sender;
- (IBAction)previewImageTapped:(id)sender;
- (IBAction)randomButtonTapped:(id)sender;

- (void)reloadFiltersOnCamera;

@end

