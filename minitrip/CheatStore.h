//
//  CheatStore.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-28.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cheat.h"

@interface CheatStore : NSObject

+ (CheatStore *)sharedInstance;

- (NSArray *)getUnlockedCheats;
- (Cheat *)codeUnlocksCheat:(NSString *)code;
- (BOOL)isSunshineGlitchEnabled;
- (BOOL)isGlitchWizardGlitchEnabled;
- (BOOL)isRandomGlitchEnabled;
- (BOOL)isClearGlitchEnabled;
- (void)saveToDefaults;
- (BOOL)isClearGlitchUnlocked;

@end
