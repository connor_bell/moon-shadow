//
//  GradientFilter.m
//  minitrip
//
//  Created by Connor Bell on 2014-12-25.
//  Copyright (c) 2014 Connor Bell. All rights reserved.
//

#import "TyeDyeFilter.h"

NSString *const kGPUImageDataDeleteFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;

 uniform lowp vec3 colorBlend;
 uniform highp float time;
 uniform lowp float sensitivity;
 uniform lowp float intensity;
 uniform lowp int blendType;
 uniform lowp float touchX;
 uniform lowp float touchY;
 uniform highp float resX;
 uniform highp float resY;
 
 lowp vec4 hue(lowp vec4 color, lowp float shift) {
     
     lowp  vec4  kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
     lowp  vec4  kRGBToI     = vec4 (0.596, -0.275, -0.321, 0.0);
     lowp  vec4  kRGBToQ     = vec4 (0.212, -0.523, 0.311, 0.0);
     
     lowp  vec4  kYIQToR   = vec4 (1.0, 0.956, 0.621, 0.0);
     lowp  vec4  kYIQToG   = vec4 (1.0, -0.272, -0.647, 0.0);
     lowp  vec4  kYIQToB   = vec4 (1.0, -1.107, 1.704, 0.0);
     
     // Convert to YIQ
     lowp float   YPrime  = dot (color, kRGBToYPrime);
     lowp float   I      = dot (color, kRGBToI);
     lowp float   Q      = dot (color, kRGBToQ);
     
     // Calculate the hue and chroma
     lowp float   hue     = atan (Q, I);
     lowp float   chroma  = sqrt (I * I + Q * Q);
     
     // Make the user's adjustments
     hue += shift;
     
     // Convert back to YIQ
     Q = chroma * sin (hue);
     I = chroma * cos (hue);
     
     // Convert back to RGB
     lowp vec4    yIQ   = vec4 (YPrime, I, Q, 0.0);
     color.r = dot (yIQ, kYIQToR);
     color.g = dot (yIQ, kYIQToG);
     color.b = dot (yIQ, kYIQToB);
     
     return color;
 }
 
 highp vec2 kale(highp vec2 uv, highp float angle, highp float base, highp float spin) {
     highp float a = atan(uv.y,uv.x)+spin;
     highp float d = length(uv);
     a = mod(a,angle*2.0);
     a = abs(a-angle);
     uv.x = sin(a+base)*d;
     uv.y = cos(a+base)*d;
     return uv;
 }
 
 highp vec2 rotate(highp float px, highp float py, highp float angle){
     highp vec2 r = vec2(0);
     r.x = cos(angle)*px - sin(angle)*py;
     r.y = sin(angle)*px + cos(angle)*py;
     return r;
 }
 
 mediump vec4 overlay(mediump vec4 base, mediump vec4 overlay) {
     mediump float ra;
     if (2.0 * base.r < base.a) {
         ra = 2.0 * overlay.r * base.r + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     } else {
         ra = overlay.a * base.a - 2.0 * (base.a - base.r) * (overlay.a - overlay.r) + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     }
     
     mediump float ga;
     if (2.0 * base.g < base.a) {
         ga = 2.0 * overlay.g * base.g + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     } else {
         ga = overlay.a * base.a - 2.0 * (base.a - base.g) * (overlay.a - overlay.g) + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     }
     
     mediump float ba;
     if (2.0 * base.b < base.a) {
         ba = 2.0 * overlay.b * base.b + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     } else {
         ba = overlay.a * base.a - 2.0 * (base.a - base.b) * (overlay.a - overlay.b) + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     }
     return vec4(ra, ga, ba, 1.0);
 }
 
 void main()
 {
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     
     lowp float maskY = 0.2989 * colorBlend.r + 0.5866 * colorBlend.g + 0.1145 * colorBlend.b;
     lowp float maskCr = 0.7132 * (colorBlend.r - maskY);
     lowp float maskCb = 0.5647 * (colorBlend.b - maskY);
     
     lowp float Y = 0.2989 * textureColor.r + 0.5866 * textureColor.g + 0.1145 * textureColor.b;
     lowp float Cr = 0.7132 * (textureColor.r - Y);
     lowp float Cb = 0.5647 * (textureColor.b - Y);
     
     lowp float blendValue = 1.0 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), vec3(maskY,maskCr, maskCb)));

     if (blendValue > 0.05) {
         //CONNOR - replace mouse with touch position going from -1 to +1
         highp vec2 touch = vec2(touchX, touchY);
         highp vec2 iResolution = vec2(resX,resY);
         
         highp vec2 fragCoord = textureCoordinate * iResolution.xy;
         fragCoord.xy -= touch*iResolution.xy;

         highp vec2 uv = fragCoord.xy / iResolution.xy * 3.;
         
         const mediump float p = 3.14159265359;
         highp float i = time*.5;
         
         //CONNOR - replace with 2 fun accel axis going from -1 - +1
         highp vec2 accel = vec2(0.5, 0.5);
         
         uv = kale(uv, p/12.0,accel.x*p*2.,accel.y*p);
         
         highp vec4 c = vec4(1.0);
         highp mat2 m = mat2(sin(uv.y*cos(uv.x+i)+i*0.1)*20.0,-6.0,sin(uv.x+i*1.5)*3.0,-cos(uv.y-i)*2.0);
         uv = rotate(uv.x,uv.y,length(uv)+i*.4);
         c.rg = cos(sin(uv.xx+uv.yy)*m-i);
         c.b = sin(rotate(uv.x,uv.x,length(uv.xx)*3.0+i).x-uv.y+i);
         c = vec4(1.0-hue(c,i).rgb,1.0);
         
         if (blendType == 0) {
             c = overlay(textureColor, c);
         }
         if (blendType == 1) {
             c = c * textureColor + c * (1.0 - textureColor.a) + textureColor * (1.0 - c.a);
         }
         
         gl_FragColor = mix(textureColor, c, blendValue);
     } else {
         gl_FragColor = textureColor;
     }
     
}
 );

@interface TyeDyeFilter () {
    GLint colorBlendUniform;
    GLint timeUniform;
    GLint intensityUniform;
    GLint sensitivityUniform;
    GLint blendTypeUniform;
    GLint touchXUniform;
    GLint touchYUniform;
    GLint resXUniform;
    GLint resYUniform;
}

@end

@implementation TyeDyeFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageDataDeleteFragmentShaderString])) {
        return nil;
    }

    colorBlendUniform = [filterProgram uniformIndex:@"colorBlend"];
    timeUniform = [filterProgram uniformIndex:@"time"];
    intensityUniform = [filterProgram uniformIndex:@"intensity"];
    sensitivityUniform = [filterProgram uniformIndex:@"sensitivity"];
    blendTypeUniform = [filterProgram uniformIndex:@"blendType"];
    touchXUniform = [filterProgram uniformIndex:@"touchX"];
    touchYUniform = [filterProgram uniformIndex:@"touchY"];
    resXUniform = [filterProgram uniformIndex:@"resX"];
    resYUniform = [filterProgram uniformIndex:@"resY"];
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    [self setFloat:(float)bounds.size.width forUniform:resXUniform program:filterProgram];
    [self setFloat:(float)bounds.size.height forUniform:resYUniform program:filterProgram];

    return self;
}

- (void)setTime:(float)t {
    _time = t;
    [self setFloat:t forUniform:timeUniform program:filterProgram];
}

- (void)setColorBlendValue:(UIColor *)colorBlendValue {
    _colorBlendValue = colorBlendValue;

    CGFloat r, g, b, a;
    [colorBlendValue getRed:&r green:&g blue:&b alpha:&a];

    GPUVector3 c;
    c.one = r;
    c.two = g;
    c.three = b;
 
    [self setVec3:c forUniform:colorBlendUniform program:filterProgram];
}

- (void)setIntensity:(float)intensity {
    _intensity = intensity;
    [self setFloat:intensity forUniform:intensity program:filterProgram];
}

- (void)setSensitivity:(float)sensitivity {
    _sensitivity = sensitivity;
    [self setFloat:_sensitivity forUniform:sensitivityUniform program:filterProgram];
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [self setInteger:_blendType forUniform:blendTypeUniform program:filterProgram];
}

- (void)setTouchX:(float)touchX {
    _touchX = touchX;
    [self setFloat:_touchX forUniform:touchXUniform program:filterProgram];
}

- (void)setTouchY:(float)touchY {
    _touchY = touchY;
    [self setFloat:_touchY forUniform:touchYUniform program:filterProgram];
}

@end