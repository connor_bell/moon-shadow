#import "ToonFilter.h"

NSString *const kGPUImageToonFragmentShader = SHADER_STRING
(
 varying lowp vec2 textureCoordinate;
 varying lowp vec2 leftTextureCoordinate;
 varying lowp vec2 rightTextureCoordinate;
 
 varying lowp vec2 topTextureCoordinate;
 varying lowp vec2 topLeftTextureCoordinate;
 varying lowp vec2 topRightTextureCoordinate;
 
 varying lowp vec2 bottomTextureCoordinate;
 varying lowp vec2 bottomLeftTextureCoordinate;
 varying lowp vec2 bottomRightTextureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform lowp vec3 colorBlend;
 uniform highp float time;
 uniform lowp float sensitivity;
 uniform lowp float intensity;
 uniform lowp int blendType;
 uniform highp float resX;
 uniform highp float resY;
 
 const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);

 const lowp float threshold = 0.25;
 const mediump float quantizationLevels = 5.0;
 
 
 mediump vec4 overlay(mediump vec4 base, mediump vec4 overlay) {
     mediump float ra;
     if (2.0 * base.r < base.a) {
         ra = 2.0 * overlay.r * base.r + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     } else {
         ra = overlay.a * base.a - 2.0 * (base.a - base.r) * (overlay.a - overlay.r) + overlay.r * (1.0 - base.a) + base.r * (1.0 - overlay.a);
     }
     
     mediump float ga;
     if (2.0 * base.g < base.a) {
         ga = 2.0 * overlay.g * base.g + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     } else {
         ga = overlay.a * base.a - 2.0 * (base.a - base.g) * (overlay.a - overlay.g) + overlay.g * (1.0 - base.a) + base.g * (1.0 - overlay.a);
     }
     
     mediump float ba;
     if (2.0 * base.b < base.a) {
         ba = 2.0 * overlay.b * base.b + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     } else {
         ba = overlay.a * base.a - 2.0 * (base.a - base.b) * (overlay.a - overlay.b) + overlay.b * (1.0 - base.a) + base.b * (1.0 - overlay.a);
     }
     return vec4(ra, ga, ba, 1.0);
 }
 
 void main() {
     
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     
     lowp float maskY = 0.2989 * colorBlend.r + 0.5866 * colorBlend.g + 0.1145 * colorBlend.b;
     lowp float maskCr = 0.7132 * (colorBlend.r - maskY);
     lowp float maskCb = 0.5647 * (colorBlend.b - maskY);
     
     lowp float Y = 0.2989 * textureColor.r + 0.5866 * textureColor.g + 0.1145 * textureColor.b;
     lowp float Cr = 0.7132 * (textureColor.r - Y);
     lowp float Cb = 0.5647 * (textureColor.b - Y);
     
     lowp float blendValue = 1.1 - smoothstep(intensity, intensity + sensitivity, distance(vec3(Y, Cr, Cb), vec3(maskY,maskCr, maskCb)));
     
     if (blendValue > 0.1) {
         lowp float bottomLeftIntensity = texture2D(inputImageTexture, bottomLeftTextureCoordinate).r;
         lowp float topRightIntensity = texture2D(inputImageTexture, topRightTextureCoordinate).r;
         lowp float topLeftIntensity = texture2D(inputImageTexture, topLeftTextureCoordinate).r;
         lowp float bottomRightIntensity = texture2D(inputImageTexture, bottomRightTextureCoordinate).r;
         lowp float leftIntensity = texture2D(inputImageTexture, leftTextureCoordinate).r;
         lowp float rightIntensity = texture2D(inputImageTexture, rightTextureCoordinate).r;
         lowp float bottomIntensity = texture2D(inputImageTexture, bottomTextureCoordinate).r;
         lowp float topIntensity = texture2D(inputImageTexture, topTextureCoordinate).r;
         lowp float h = -topLeftIntensity - 2.0 * topIntensity - topRightIntensity + bottomLeftIntensity + 2.0 * bottomIntensity + bottomRightIntensity;
         lowp float v = -bottomLeftIntensity - 2.0 * leftIntensity - topLeftIntensity + bottomRightIntensity + 2.0 * rightIntensity + topRightIntensity;
         
         lowp float mag = length(vec2(h, v));
         
         lowp vec3 posterizedImageColor = floor((textureColor.rgb * quantizationLevels) + 0.5) / quantizationLevels;
         
         lowp float thresholdTest = 1.0 - step(threshold, mag);
         
         lowp vec4 c = vec4(posterizedImageColor * thresholdTest, textureColor.a);

         if (blendType == 0) {
             c = overlay(c, textureColor);
         }
         if (blendType == 1) {
             c = c * textureColor + c * (1.0 - textureColor.a) + textureColor * (1.0 - c.a);
         }
         
         gl_FragColor = mix(textureColor, c, blendValue);
     } else {
         gl_FragColor = textureColor;
     }
 }
 
 );


@interface ToonFilter () {
    GLint colorBlendUniform;
    GLint timeUniform;
    GLint intensityUniform;
    GLint sensitivityUniform;
    GLint blendTypeUniform;
    GLint resXUniform;
    GLint resYUniform;
}

@end

@implementation ToonFilter

- (id)init {
    if (!(self = [super initWithFragmentShaderFromString:kGPUImageToonFragmentShader])) {
        return nil;
    }
    
    colorBlendUniform = [filterProgram uniformIndex:@"colorBlend"];
    timeUniform = [filterProgram uniformIndex:@"time"];
    intensityUniform = [filterProgram uniformIndex:@"intensity"];
    sensitivityUniform = [filterProgram uniformIndex:@"sensitivity"];
    blendTypeUniform = [filterProgram uniformIndex:@"blendType"];
    resXUniform = [filterProgram uniformIndex:@"resX"];
    resYUniform = [filterProgram uniformIndex:@"resY"];

    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    [self setFloat:(float)bounds.size.width forUniform:resXUniform program:filterProgram];
    [self setFloat:(float)bounds.size.height forUniform:resYUniform program:filterProgram];
    
    return self;
}

- (void)setTime:(float)t {
    _time = t;
    [self setFloat:t forUniform:timeUniform program:filterProgram];
}

- (void)setColorBlendValue:(UIColor *)colorBlendValue {
    _colorBlendValue = colorBlendValue;
    
    CGFloat r, g, b, a;
    [colorBlendValue getRed:&r green:&g blue:&b alpha:&a];
    
    GPUVector3 c;
    c.one = r;
    c.two = g;
    c.three = b;
    
    [self setVec3:c forUniform:colorBlendUniform program:filterProgram];
}

- (void)setIntensity:(float)intensity {
    _intensity = intensity;
    [self setFloat:intensity forUniform:intensity program:filterProgram];
}

- (void)setSensitivity:(float)sensitivity {
    _sensitivity = sensitivity;
    [self setFloat:_sensitivity forUniform:sensitivityUniform program:filterProgram];
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [self setInteger:_blendType forUniform:blendTypeUniform program:filterProgram];
}

@end
