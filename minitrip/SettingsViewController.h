//
//  SettingsViewController.h
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-23.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "ViewController.h"

@interface SettingsViewController : UITableViewController <MFMailComposeViewControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) IBOutlet UISwitch *audioEnabledSwitch;
@property (nonatomic, strong) IBOutlet UISwitch *touchSampleEnabledSwitch;
@property (nonatomic, strong) IBOutlet UILabel *selectedVideoSettingLabel;

- (IBAction)audioSwitchTapped:(id)sender;
- (IBAction)touchSamplePosTapped:(id)sender;

@end
