//
//  PreviewImageViewController.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-05-01.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "PreviewImageViewController.h"

@interface PreviewImageViewController ()

@end

@implementation PreviewImageViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _imageView.image = _previewImage;
    _imageScrollView.contentSize = self.view.frame.size;
    _imageScrollView.zoomScale = 1.0f;
    [self centerScrollViewContents];
}

#pragma mark - Actions

- (void)doneTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return _imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    [self centerScrollViewContents];
}

- (void)centerScrollViewContents {
    CGSize boundsSize = _imageScrollView.bounds.size;
    CGRect contentsFrame = _imageView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    _imageView.frame = contentsFrame;
}

@end
