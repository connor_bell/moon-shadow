#import "GPUImage3x3TextureSamplingFilter.h"
#import "Effect.h"

@interface ToonFilter : GPUImage3x3TextureSamplingFilter

@property(readwrite, nonatomic) UIColor *colorBlendValue;
@property(readwrite, nonatomic) BlendType blendType;
@property(readwrite, nonatomic) float intensity;
@property(readwrite, nonatomic) float sensitivity;
@property(readwrite, nonatomic) float time;

@end
