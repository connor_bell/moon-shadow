#import "Spirals2Effect.h"
#import "Spirals2Filter.h"

@interface Spirals2Effect () {
    Spirals2Filter *_imageFilter;
}
@end

@implementation Spirals2Effect
@synthesize currentColor=_currentColor;
@synthesize blendType=_blendType;

- (instancetype)init {
    if (self = [super init]) {
        _imageFilter = [Spirals2Filter new];
        [_imageFilter setSensitivity:EFFECT_START_VAL];
    }
    return self;
}

- (NSString *)title {
    return @"SPIRALS";
}

- (UIImage *)previewImage {
    return [UIImage imageNamed:@"WATER"];
}

- (void)updateTimewithValue:(float)value {
    [_imageFilter setTime:value];
}

- (void)updateSensitivityWithValue:(float)value {
    [_imageFilter setSensitivity:value];
}

- (void)setCurrentColor:(UIColor *)currentColor {
    _currentColor = currentColor;
    [_imageFilter setColorBlendValue:_currentColor];
}

- (GPUImageFilter *)imageFilter {
    return _imageFilter;
}

- (float)getIntensity {
    return _imageFilter.sensitivity;
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [_imageFilter setBlendType:blendType];
}

- (void)setTouchPos:(CGPoint)touchPos {
    _imageFilter.touchX = touchPos.x;
    _imageFilter.touchY = touchPos.y;
}

@end
