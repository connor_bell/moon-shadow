//
//  EffectCell.h
//  minitrip
//
//  Created by Connor Bell on 2015-04-13.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Effect.h"

@interface EffectCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UIImageView *selectedEffectImageView;
@property (nonatomic, assign) Effect *effect;

@end
