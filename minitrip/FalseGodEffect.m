#import "FalseGodEffect.h"
#import "FalseGodFilter.h"

@interface FalseGodEffect () {
    FalseGodFilter *_imageFilter;
}
@end

@implementation FalseGodEffect
@synthesize currentColor=_currentColor;
@synthesize blendType=_blendType;
- (instancetype)init {
    if (self = [super init]) {
        _imageFilter = [FalseGodFilter new];
        [_imageFilter setSensitivity:EFFECT_START_VAL];
    }
    return self;
}

- (NSString *)title {
    return @"FLSEGOD";
}

- (UIImage *)previewImage {
    return [UIImage imageNamed:@"FALSEGOD"];
}

- (void)updateTimewithValue:(float)value {
    [_imageFilter setTime:value];
}

- (void)updateSensitivityWithValue:(float)value {
    [_imageFilter setSensitivity:value];
}

- (void)setCurrentColor:(UIColor *)currentColor {
    _currentColor = currentColor;
    [_imageFilter setColorBlendValue:_currentColor];
}

- (GPUImageFilter *)imageFilter {
    return _imageFilter;
}

- (float)getIntensity {
    return _imageFilter.sensitivity;
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [_imageFilter setBlendType:blendType];
}

@end
