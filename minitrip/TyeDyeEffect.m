//
//  TyeDyeEffect.m
//  minitrip
//
//  Created by Connor Bell on 2015-04-12.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "TyeDyeEffect.h"
#import "TyeDyeFilter.h"
#import "UIHelper.h"

@interface TyeDyeEffect () {
    TyeDyeFilter *_imageFilter;
    
    UIColor *_highColor;
    UIColor *_lowColor;
    
    NSTimer *_colorTimer;
    float _t;
}

@end

@implementation TyeDyeEffect
@synthesize currentColor=_currentColor;
@synthesize blendType=_blendType;

- (instancetype)init {
    if (self = [super init]) {
        _imageFilter = [TyeDyeFilter new];
        [_imageFilter setSensitivity:EFFECT_START_VAL];
    }
    return self;
}

- (NSString *)title {
    return @"CALE";
}

- (UIImage *)previewImage {
    return [UIImage imageNamed:@"NETGRIND"];
}

- (void)updateTimewithValue:(float)value {
    [_imageFilter setTime:value];
}

- (void)updateSensitivityWithValue:(float)value {
    [_imageFilter setSensitivity:value];
}

- (void)interpolateColor {
    _t += 1.0/10.0;

    _currentColor = [UIHelper interpolate:_lowColor c2:_highColor t:_t];
    [_imageFilter setColorBlendValue:_currentColor];

    if (_t >= 1.0) {
        [_colorTimer invalidate];
        _colorTimer = nil;
        _currentColor = _highColor;
        [_imageFilter setColorBlendValue:_currentColor];
    }
}

- (void)setCurrentColor:(UIColor *)currentColor {
    
    _highColor = currentColor;
    _lowColor = _currentColor;
    
    _t = 0.0f;
    [_colorTimer invalidate];
    _colorTimer = nil;
    
    _colorTimer = [NSTimer scheduledTimerWithTimeInterval:(1.0/20.0) target:self selector:@selector(interpolateColor) userInfo:nil repeats:YES];
    [_colorTimer fire];
}

- (GPUImageFilter *)imageFilter {
    return _imageFilter;
}

- (float)getIntensity {
    return _imageFilter.sensitivity;
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [_imageFilter setBlendType:blendType];
}

- (void)setTouchPos:(CGPoint)touchPos {
    _imageFilter.touchX = touchPos.x;
    _imageFilter.touchY = touchPos.y;
}

@end
