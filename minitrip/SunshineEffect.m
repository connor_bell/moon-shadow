#import "SunshineEffect.h"
#import "SunshineFilter.h"

@interface SunshineEffect () {
    SunshineFilter *_imageFilter;
}
@end

@implementation SunshineEffect
@synthesize currentColor=_currentColor;
@synthesize blendType=_blendType;

- (instancetype)init {
    if (self = [super init]) {
        _imageFilter = [SunshineFilter new];
        [_imageFilter setSensitivity:EFFECT_START_VAL];
    }
    return self;
}

- (NSString *)title {
    return @"SUNSHINE";
}

- (UIImage *)previewImage {
    return [UIImage imageNamed:@"SUNSHINE"];
}

- (void)updateTimewithValue:(float)value {
    [_imageFilter setTime:value];
}

- (void)updateSensitivityWithValue:(float)value {
    [_imageFilter setSensitivity:value];
}

- (void)setCurrentColor:(UIColor *)currentColor {
    _currentColor = currentColor;
    [_imageFilter setColorBlendValue:_currentColor];
}

- (GPUImageFilter *)imageFilter {
    return _imageFilter;
}

- (float)getIntensity {
    return _imageFilter.sensitivity;
}

- (void)setBlendType:(BlendType)blendType {
    _blendType = blendType;
    [_imageFilter setBlendType:blendType];
}

- (void)setTouchPos:(CGPoint)touchPos {
    _imageFilter.touchX = touchPos.x;
    _imageFilter.touchY = touchPos.y;
}

@end
