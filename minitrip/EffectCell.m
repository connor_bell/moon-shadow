    //
//  EffectCell.m
//  minitrip
//
//  Created by Connor Bell on 2015-04-13.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "EffectCell.h"

@implementation EffectCell

- (id)initWithFrame:(CGRect)frame{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSString *)reuseIdentifier {
    return @"EffectCell";
}

@end
