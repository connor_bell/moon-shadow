//
//  SettingsStore.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-23.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "SettingsStore.h"
#import "UIHelper.h"

static SettingsStore *instance = nil;
static NSString *MICROPHONE_AUDIO_KEY = @"MICROPHONE_AUDIO_KEY";
static NSString *VIDEO_QUALITY_KEY = @"VIDEO_QUALITY_KEY";
static NSString *TOUCH_SAMPLE_POS_KEY = @"TOUCH_SAMPLE_POS_KEY";

@implementation SettingsStore

+ (SettingsStore *)sharedInstance {
    @synchronized(self) {
        if (!instance) {
            instance = [SettingsStore new];
        }
    }
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        _microphoneAudioEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:MICROPHONE_AUDIO_KEY];
        if (![[NSUserDefaults standardUserDefaults] objectForKey:VIDEO_QUALITY_KEY] && [UIHelper highQualityVideoSupported]) {
            self.videoQuality = VideoQualityHigh;
        } else {
            _videoQuality = (VideoQuality)[[NSUserDefaults standardUserDefaults] integerForKey:VIDEO_QUALITY_KEY];
        }
        
        if (![[NSUserDefaults standardUserDefaults] objectForKey:TOUCH_SAMPLE_POS_KEY]) {
            self.touchSamplesOriginalImage = YES;
        } else {
            _touchSamplesOriginalImage = [[NSUserDefaults standardUserDefaults] integerForKey:TOUCH_SAMPLE_POS_KEY];
        }
        
    }
    return self;
}

- (void)setMicrophoneAudioEnabled:(BOOL)microphoneAudioEnabled {
    _microphoneAudioEnabled = microphoneAudioEnabled;
    [[NSUserDefaults standardUserDefaults] setBool:_microphoneAudioEnabled forKey:MICROPHONE_AUDIO_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setVideoQuality:(VideoQuality)videoQuality {
    _videoQuality = videoQuality;
    [[NSUserDefaults standardUserDefaults] setBool:_videoQuality forKey:VIDEO_QUALITY_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setTouchSamplesOriginalImage:(BOOL)touchSamplesOriginalImage {
    _touchSamplesOriginalImage = touchSamplesOriginalImage;
    [[NSUserDefaults standardUserDefaults] setBool:_touchSamplesOriginalImage forKey:TOUCH_SAMPLE_POS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
