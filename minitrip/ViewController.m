//
//  ViewController.m
//  minitrip
//
//  Created by Connor Bell on 2014-12-25.
//  Copyright (c) 2014 Connor Bell. All rights reserved.
//

#import "ViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <ImageIO/ImageIO.h>

#import "PreviewImageViewController.h"
#import "SettingsStore.h"
#import "EffectManager.h"
#import "EffectCell.h"
#import "Effect.h"
#import "UIHelper.h"
#import "CheatStore.h"

#define ARC4RANDOM_MAX      0x100000000

static NSString *VIDEO_MODE_IMAGE_NAME = @"VideoModeButton";
static NSString *PHOTO_MODE_IMAGE_NAME = @"PhotoModeButton";
static NSString *LIBRARY_MODE_IMAGE_NAME = @"LibraryButton";
static NSString *SWITCH_CAMERA_IMAGE_NAME = @"SwitchCameraButton";
static NSString *VIDEO_RECORDING_STOP_BUTTON = @"StopButton";
static NSString *CAMERA_CAPTURE_BUTTON = @"Camera Button";
static NSString *VIDEO_RECORDING_START_BUTTON = @"StartRecordingButton";

static NSString *SEGUE_ID_SETTINGS = @"SEGUE_ID_SETTINGS";
static NSString *SEGUE_ID_PREVIEW = @"SEGUE_ID_PREVIEW";

@interface ViewController () {
    NSArray *_effects;
    UIImagePickerController *_imagePicker;
    NSMutableArray *_activeEffects;
    UIAlertView *_clearContextAlertView;
    Effect *_selectedEffect;
    BOOL _controlsViewShowing;
    BOOL _frontCamera;
    BOOL _highQualitySupported;
    BOOL _isRecording;
    BOOL _isConfiguringCamera;
    BOOL _isTouchSampleOriginalFeed;
    BOOL _isPreviewingPhoto;
    CGSize _importedVideoResolution;
    MoonShadowMode _moonShadowMode;
    NSURL *_importedVideoUrl;

    GPUImageFilterPipeline *_pipeline;
    
    GPUImagePicture *_stillImageSource;
    UIImage *_inputImage;
    GPUImageFilter *_emptyFilter;
    GPUImageMovieWriter *_movieWriter;
    GPUImageMovie *_playingMovie;
    GPUImageMovie *_exportingMovie;
    UITapGestureRecognizer *_colorMatchGesture;
    GPUImageRawDataOutput *_rawDataOutput;
    
    CGPoint _tapLocation;
    NSTimer *_timeUpdateTimer;
    NSTimer *_videoTimer;
    int _recordingProgresstime;
    
    UIImage *_latestImageSaved;
    NSDate *_startTimerDate;
    ALAssetsLibrary *_assetsLibrary;
    AVPlayer *_player;
    AVPlayerItem *_playerItem;
    
    NSTimeInterval _timeWhenPreviewTapped;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_captureButton setExclusiveTouch:YES];
    [_videoModeButton setExclusiveTouch:YES];
    [_videoModeButton setExclusiveTouch:YES];
    [_switchCameraButton setExclusiveTouch:YES];
    [_settingsButton setExclusiveTouch:YES];
    
    [_infoView showIfFirstLaunch];
    [_effectCollectionView registerClass:[EffectCell class] forCellWithReuseIdentifier:@"EffectCell"];
    
    _stillImageExportView.hidden = YES;
    _videoTimerLabel.hidden = YES;
    _highQualitySupported = [UIHelper highQualityVideoSupported];
    _activeEffects = [NSMutableArray array];
    _emptyFilter = [GPUImageFilter new];
    _assetsLibrary = [[ALAssetsLibrary alloc] init];
    
    _colorMatchGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImagePoint:)];
    
    [_imageFilterView addGestureRecognizer:_colorMatchGesture];
    _moonShadowMode = VideoMode;
    [self updateVideoModeButton];
    
    [CheatStore sharedInstance];
    [self observeSystemNotifications];
    [self resetTime];
    
    if (![GPUImageVideoCamera isBackFacingCameraPresent]) {
        _switchCameraButton.enabled = NO;
    }
}

- (void)observeSystemNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enterBackground)
                                                 name:NOTIFICATION_BACKGROUND_ENTER
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(enterForeground)
                                                 name:NOTIFICATION_BACKGROUND_EXIT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)orientationChanged:(NSNotification *)notification {
    [UIView animateWithDuration:0.25 animations:^{
        
        CGAffineTransform transform;
        
        switch ([[UIDevice currentDevice] orientation]) {
            case UIInterfaceOrientationLandscapeLeft:
                transform = CGAffineTransformMakeRotation(-M_PI/2.0);
                break;
            case UIInterfaceOrientationLandscapeRight:
                transform = CGAffineTransformMakeRotation(M_PI/2.0);
                break;
            default:
                transform = CGAffineTransformMakeRotation(0.0);
                break;
        }
        
        _switchCameraButton.transform = transform;
        _videoModeView.transform = transform;
        _settingsButton.transform = transform;
        _colorView.superview.transform = transform;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:SEGUE_ID_PREVIEW]) {
        PreviewImageViewController *controller = segue.destinationViewController;
        controller.previewImage = sender;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _effects = [[EffectManager sharedInstance] effects];
    [[EffectManager sharedInstance] modifyEffectsForActiveCheats];
    [_effectCollectionView reloadData];
    _isTouchSampleOriginalFeed = [[SettingsStore sharedInstance] touchSamplesOriginalImage];
    [self reloadCameraSettings];
    _randomButton.hidden = ![[CheatStore sharedInstance] isRandomGlitchEnabled];
}

- (void)enterBackground {
    if (_isRecording) {
        [self finishRecording];
        [_stillCamera stopCameraCapture];
        [self resetTime];
    }
    if (_player) {
        [_player pause];
    }
}

- (void)enterForeground {
    [self reloadFiltersOnCamera];
    if (_player) {
        [_player play];
    }
}

- (void)resetTime {
    _startTimerDate = [NSDate date];
    [_timeUpdateTimer invalidate];
    _timeUpdateTimer = nil;
}

#pragma mark - Actions

- (IBAction)captureButtonTapped {
    if ([_activeEffects count] > 0) {
        if (_moonShadowMode == VideoMode || _moonShadowMode == ImportMediaMode) {
            [self toggleRecording];
        } else {
            [self takePhoto];
        }
    } else {
        [self flashEffectsCollectionView];
    }
}

- (void)takePhoto {

    _timeControlSlider.value = 0.5;
    [_timeUpdateTimer invalidate];
    _timeUpdateTimer = nil;
    _timeWhenPreviewTapped = [[NSDate date] timeIntervalSinceDate:_startTimerDate];
    
    [_stillCamera capturePhotoAsJPEGProcessedUpToFilter:[_pipeline.filters firstObject]
                                        withOrientation:[self imageOutputOrientation]
                                  withCompletionHandler:^(NSData *processedJPEG, NSError *error){

                                      runOnMainQueueWithoutDeadlocking(^{
                                          _inputImage = [UIHelper rotateUIImage:[UIImage imageWithData:processedJPEG]
                                                                            dir:[[UIDevice currentDevice] orientation]];
                                          
                                          [_stillCamera removeAllTargets];
                                          [_stillCamera stopCameraCapture];
                                          _stillCamera = nil;
                                          
                                          if (_controlsViewShowing) {
                                              [self toggleControlsViewTapped:nil];
                                          }
                                          _bottomContainerView.hidden = YES;
                                          _previewControlsContainerView.hidden = NO;
                                          [self reloadFiltersOnStillImageAndStartTimer:NO];
                                          [self updateTimeWithInterval:_timeWhenPreviewTapped];
                                      });
                                  }];
    
}

- (IBAction)previewSliderChanged:(UISlider *)slider {
    float offset = (slider.value - 0.5)*4.0;
    NSTimeInterval newTime = (_timeWhenPreviewTapped + offset);
    [self updateTimeWithInterval:newTime];
}

- (IBAction)saveImageTapped:(id)sender {
    [_loadingOverlay show];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void) {
        [[[_activeEffects lastObject] imageFilter] useNextFrameForImageCapture];
        [_stillImageSource processImage];
        _latestImageSaved = [[[_activeEffects lastObject] imageFilter] imageFromCurrentFramebuffer];
        [self saveImageToCameraRoll:_latestImageSaved];
        [self clearStillImageSource];
    });
}

- (IBAction)cancelSaveImageTapped:(id)sender {
    _previewControlsContainerView.hidden = YES;
    _bottomContainerView.hidden = NO;
    [self clearStillImageSource];
    [self reloadCameraSettings];
}

- (void)saveImageToCameraRoll:(UIImage *)image {
    [_assetsLibrary writeImageToSavedPhotosAlbum:[image CGImage] metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
        runOnMainQueueWithoutDeadlocking(^{
            [_loadingOverlay hide];
            if (error) {
                [_infoView showError:@"ERROR: the image failed to save"];
            } else {
                [_infoView showImageSaved];
            }
            _previewControlsContainerView.hidden = YES;
            _bottomContainerView.hidden = NO;
            [_captureButton setEnabled:YES];
            if (_moonShadowMode == ImageMode) {
                [self clearStillImageSource];
                [self reloadCameraSettings];
            }
        });
    }];
}

- (void)saveVideo {
    [_loadingOverlay show];
    UISaveVideoAtPathToSavedPhotosAlbum([[self moviePath] path], self, @selector(video:didFinishSavingWithError:contextInfo:), NULL);
}

- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    runOnMainQueueWithoutDeadlocking(^{
        if (error) {
            [self.infoView showError:@"ERROR: the video failed to save"];
        } else {
            [self.infoView showVideoSaved];
        }
        if (_moonShadowMode == ImportMediaMode) {
            [self playMovieAtUrl:_importedVideoUrl];
        } else {
            [_playingMovie removeAllTargets];
            [_playingMovie cancelProcessing];
            [self clearPlayerItems];
            _playingMovie = nil;
            [_stillCamera startCameraCapture];
        
        }

        [self showBottomControlView];
        _stillImageExportView.hidden = YES;
        [self reloadFiltersOnCamera];
        [_loadingOverlay hide];
        [_imageFilterView addGestureRecognizer:_colorMatchGesture];
    });
}

- (NSURL *)moviePath {
    return [[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
                                                   inDomain:NSUserDomainMask
                                          appropriateForURL:nil
                                                     create:YES
                                                      error:nil] URLByAppendingPathComponent:@"moonshadow.mov"];
}

- (IBAction)switchCameraTapped:(id)sender {
    if (_moonShadowMode == ImportMediaMode) {
        [self importVideo];
    } else {
        if (_isConfiguringCamera) return;
        _frontCamera = !_frontCamera;
        [self reloadCameraSettings];
        [self reloadFiltersOnCamera];
    }
}

- (void)reloadCameraSettings {
    
    if (_moonShadowMode == ImportMediaMode) {
        [_stillCamera removeAllTargets];
        _stillCamera = nil;
        return;
    }

    if (_isConfiguringCamera) {
        return;
    }
    
    _isConfiguringCamera = YES;
    
    [_stillCamera stopCameraCapture];
    
    NSString *sessionPreset;
    if (_highQualitySupported) {
        sessionPreset = [[SettingsStore sharedInstance] videoQuality] == VideoQualityHigh ? AVCaptureSessionPresetHigh : AVCaptureSessionPresetMedium;
    } else {
        sessionPreset = [[SettingsStore sharedInstance] videoQuality] == VideoQualityHigh ? AVCaptureSessionPresetMedium : AVCaptureSessionPresetLow;
    }
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if (status == AVAuthorizationStatusDenied) {
        UIAlertView *permissionError = [[UIAlertView alloc] initWithTitle:@"Permission Error" message:@"You have to grant MOON SHADOW permission to access the camera via Settings." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [permissionError show];
    }
    
    if (_frontCamera) {
        _stillCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:sessionPreset
                                                           cameraPosition:AVCaptureDevicePositionFront];
    } else {
        _stillCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:sessionPreset
                                                           cameraPosition:AVCaptureDevicePositionBack];
    }
    
    [_stillCamera forceProcessingAtSizeRespectingAspectRatio:[self outputSizeForOrientation]];
    [self updateImageViewConstaintsForCameraMode];
    _stillCamera.jpegCompressionQuality = 1.0;
    _stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
    _stillCamera.horizontallyMirrorFrontFacingCamera = YES;
    [_stillCamera startCameraCapture];
    [_stillCamera addTarget:_emptyFilter];
    [self reloadFiltersOnCamera];
    _isConfiguringCamera = NO;
}

- (IBAction)alphaSliderChanged:(id)sender {
    [_selectedEffect updateSensitivityWithValue:[(UISlider *)sender value]];
}

- (void)reloadFiltersOnStillImageAndStartTimer:(BOOL)startTimer {
    
    if (_timeUpdateTimer) {
        [self resetTime];
    }
    
    [_stillImageSource removeAllTargets];
    [_emptyFilter removeAllTargets];
    [_imageFilterView endProcessing];
    
    _stillImageSource = [[GPUImagePicture alloc] initWithImage:_inputImage];
    
    GPUImageFilter *last = _emptyFilter;
    [_emptyFilter forceProcessingAtSize:_inputImage.size];
    [_stillImageSource addTarget:last];
    
    for (Effect *filter in _activeEffects) {
        [[filter imageFilter] removeAllTargets];
        [last addTarget:[filter imageFilter]];
        last = [filter imageFilter];
    }

    [last addTarget:_imageFilterView];
    [self addRawDataFeed];

    if ([_activeEffects count] > 0 && startTimer) {
        _timeUpdateTimer = [NSTimer timerWithTimeInterval:(1.0/20.0) target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timeUpdateTimer forMode:NSRunLoopCommonModes];
    } else {
        [_emptyFilter useNextFrameForImageCapture];
        [_stillImageSource processImage];
    }
}

- (void)reloadFiltersOnCamera {
    if (_moonShadowMode == ImportMediaMode && _stillImageSource) {
        [self reloadFiltersOnStillImageAndStartTimer:YES];
        return;
    }
    
    NSMutableArray *filters = [NSMutableArray arrayWithObjects:_emptyFilter, nil];
    
    for (int i = 0; i < [_activeEffects count]; i++) {
        [filters addObject:[_activeEffects[i] imageFilter]];
    }
    
    GPUImageOutput *inputSource;
    
    if (_moonShadowMode == ImportMediaMode) {
        inputSource = _playingMovie;
    } else {
        inputSource = _stillCamera;
    }

    _pipeline = [[GPUImageFilterPipeline alloc] initWithOrderedFilters:filters
                                                                 input:inputSource
                                                                output:_imageFilterView];

    //[_imageFilterView setInputRotation:[self rotationModeForOrientation] atIndex:0];
    [[[_pipeline filters] firstObject] forceProcessingAtSizeRespectingAspectRatio:[self outputSizeForOrientation]];
    
    [self addRawDataFeed];
    if (_timeUpdateTimer) {
        [self resetTime];
    }
    
    if ([filters count] > 0) {
        _timeUpdateTimer = [NSTimer timerWithTimeInterval:(1.0/20.0) target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timeUpdateTimer forMode:NSRunLoopCommonModes];
    }
}

- (void)addRawDataFeed {
    _rawDataOutput = [[GPUImageRawDataOutput alloc] initWithImageSize:[self outputSizeForOrientation] resultsInBGRAFormat:YES];
    
    if (_isTouchSampleOriginalFeed) {
        [[[_activeEffects firstObject] imageFilter] addTarget:_rawDataOutput];
    } else {
        [[[_activeEffects lastObject] imageFilter] addTarget:_rawDataOutput];
    }
}

- (void)updateTime {
    NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:_startTimerDate];
    [self updateTimeWithInterval:time];
}

- (void)updateTimeWithInterval:(NSTimeInterval)interval {

    for (Effect *effect in _activeEffects) {
        [effect updateTimewithValue:interval];
    }
    if (_stillImageSource) {
        [_emptyFilter useNextFrameForImageCapture];
        [_stillImageSource processImage];
    }
}

- (IBAction)saveStillImageTapped:(id)sender {
    [self saveVideo];
}

- (void)clearPlayerItems {
    _player = nil;
    _playerItem = nil;
}

- (IBAction)cancelStillImageTapped:(id)sender {
    [self clearPlayerItems];
    if (_moonShadowMode == ImportMediaMode) {
        [self playMovieAtUrl:_importedVideoUrl];
    } else {
        [_playingMovie removeAllTargets];
        [_playingMovie cancelProcessing];
        _playingMovie = nil;
        [_stillCamera startCameraCapture];
    }
    [self reloadFiltersOnCamera];
    _stillImageExportView.hidden = YES;
    [self showBottomControlView];
    [_imageFilterView addGestureRecognizer:_colorMatchGesture];
}

- (void)toggleRecording {
    if (_isRecording) {
        [self finishRecording];
    } else {
        [self startRecording];
    }
}

- (void)finishRecording {
    _isRecording = NO;
    [_captureButton setImage:[UIImage imageNamed:VIDEO_RECORDING_START_BUTTON] forState:UIControlStateNormal];
    
    if (_moonShadowMode == ImportMediaMode && [[SettingsStore sharedInstance]microphoneAudioEnabled]) {
        _exportingMovie.delegate = nil;
        [self didCompletePlayingMovie];
        _switchCameraButton.enabled = YES;
        _videoModeButton.enabled = YES;
    } else {
        [_videoTimer invalidate];
        _videoTimer = nil;
        _videoTimerLabel.text = @"00:00";
        _videoTimerLabel.hidden = YES;
        [self resetTime];
        _bottomContainerView.hidden = YES;
        _recordingProgresstime = 0;
        [_imageFilterView removeGestureRecognizer:_colorMatchGesture];
        
        if (_controlsViewShowing) {
            [self toggleControlsViewTapped:nil];
        }
        
        [_movieWriter finishRecordingWithCompletionHandler:^{
            runOnMainQueueWithoutDeadlocking(^{
                _switchCameraButton.enabled = YES;
                _videoModeButton.enabled = YES;
                _stillCamera.audioEncodingTarget = nil;
                _stillImageExportView.hidden = NO;
                [_stillCamera stopCameraCapture];
                [self playMovieAtUrl:[self moviePath]];
            });
        }];
    }
}

- (void)playMovieAtUrl:(NSURL *)url {
    [_playingMovie removeAllTargets];
    [_playingMovie cancelProcessing];
    _playingMovie = nil;
    if (url) {
        _player = [AVPlayer new];
        _playerItem = [[AVPlayerItem alloc] initWithURL:url];
        [_player replaceCurrentItemWithPlayerItem:_playerItem];
        
        _playingMovie = [[GPUImageMovie alloc] initWithPlayerItem:_playerItem];
        _playingMovie.shouldRepeat = YES;
        _playingMovie.playAtActualSpeed = YES;
        [_playingMovie forceProcessingAtSize:[self outputSizeForOrientation]];
        [_playingMovie addTarget:_imageFilterView];
        [_playingMovie startProcessing];
        [_player play];

        _player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[_player currentItem]];
    }
}

- (void)processMovieAtUrl:(NSURL *)url {
    [self clearPlayerItems];
    [_loadingOverlay show];
    [_playingMovie cancelProcessing];
    _playingMovie = nil;
    [_movieWriter cancelRecording];
    _movieWriter = nil;
    
    [[[_pipeline filters] lastObject] removeTarget:_imageFilterView];

    _exportingMovie = [[GPUImageMovie alloc] initWithURL:url];
    _exportingMovie.delegate = self;
    
    [_exportingMovie addTarget:[[_pipeline filters] firstObject]];
    [[NSFileManager defaultManager] removeItemAtURL:[self moviePath] error:nil];
    
    _movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:[self moviePath] size:_importedVideoResolution];
    [[[_pipeline filters] lastObject] addTarget:_imageFilterView];
    [[[_pipeline filters] lastObject] addTarget:_movieWriter];
    _movieWriter.shouldPassthroughAudio = YES;
    _exportingMovie.audioEncodingTarget = _movieWriter;
    [_exportingMovie enableSynchronizedEncodingUsingMovieWriter:_movieWriter];
    
    [_movieWriter startRecording];
    [_exportingMovie startProcessing];
}

- (void)startRecording {
    _isRecording = YES;
    _videoModeButton.enabled = NO;
    _switchCameraButton.enabled = NO;
    [_captureButton setImage:[UIImage imageNamed:VIDEO_RECORDING_STOP_BUTTON] forState:UIControlStateNormal];

    if (_moonShadowMode == ImportMediaMode && [[SettingsStore sharedInstance] microphoneAudioEnabled]) {
        [self processMovieAtUrl:_importedVideoUrl];
        
    } else {
        
        _videoTimerLabel.hidden = NO;
        
        [[NSFileManager defaultManager] removeItemAtURL:[self moviePath] error:nil];
        CGSize outputSize;
        GPUImageRotationMode rotationMode;

        if (_moonShadowMode == ImportMediaMode) {
            rotationMode = kGPUImageNoRotation;
            outputSize = _stillImageSource ? _inputImage.size : _importedVideoResolution;
        } else {
            rotationMode = [self rotationModeForOrientation];
            outputSize = [self outputSizeForOrientation];
        }
        
        _movieWriter = [[GPUImageMovieWriter alloc] initWithMovieURL:[self moviePath] size:outputSize];

        [_movieWriter setInputRotation:rotationMode atIndex:0];
        
        _movieWriter.encodingLiveVideo = YES;
        
        [[[_pipeline filters] lastObject] addTarget:_movieWriter];
        
        if ([[SettingsStore sharedInstance] microphoneAudioEnabled]) {
            _stillCamera.audioEncodingTarget = _movieWriter;
        }
        [_movieWriter startRecording];
        _videoTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateVideoTime) userInfo:nil repeats:YES];
    }
}

- (IBAction)toggleVideoModeTapped:(id)sender {
    _moonShadowMode = (_moonShadowMode + 1) % 3;
    [self updateVideoModeButton];
    [self clearPlayerItems];
    if (_moonShadowMode == ImportMediaMode) { // this doesn't take long so don't show loading view
        [self reloadCameraSettings];    
    } else {
        [_loadingOverlay show];
        runAsynchronouslyOnVideoProcessingQueue(^{
            [self reloadCameraSettings];
            runOnMainQueueWithoutDeadlocking(^{
                [_loadingOverlay hide];
           });
        });
    }
}

- (IBAction)settingsButtonTapped:(id)sender {
    [_stillCamera stopCameraCapture];
    [self performSegueWithIdentifier:SEGUE_ID_SETTINGS sender:nil];
}

- (IBAction)blendModeChanged:(id)sender {
    [_selectedEffect setBlendType:(BlendType)_blendModeControl.selectedSegmentIndex];
}

- (IBAction)shareTapped:(id)sender {
    NSArray *activityItems;
    if (_infoView.latestSaveVideo) {
        activityItems = @[[self moviePath]];
    } else {
        activityItems = @[_latestImageSaved];
    }
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}

- (IBAction)previewImageTapped:(id)sender {
    [_stillCamera stopCameraCapture];
    [self performSegueWithIdentifier:SEGUE_ID_PREVIEW sender:_latestImageSaved];
}

- (IBAction)donePreviewTapped:(id)sender {
    _stillImageExportView.hidden = YES;
}

- (IBAction)randomButtonTapped:(id)sender {
    if (_isRecording) return;
    [_activeEffects removeAllObjects];

    if (_controlsViewShowing) {
        [self toggleControlsViewTapped:nil];
    }

    if (_selectedEffect) {
        _selectedEffect = nil;
    }
    
    int numFilters = (arc4random() % 5) + 3;
    int i = 0;
    
    while (i < numFilters) {
        Effect *effect = _effects[(arc4random() % [_effects count])];
        effect.blendType = arc4random() % 2;
        if (effect.blendType == 1) effect.blendType = 2;
        
        CGFloat brightness = (double)arc4random() / ARC4RANDOM_MAX;
        CGFloat sensitivity = ((((double)arc4random() / ARC4RANDOM_MAX)*0.5)+0.5)*0.5;

        [effect setCurrentColor:[UIColor colorWithRed:brightness green:brightness blue:brightness alpha:1.0]];
        [effect updateSensitivityWithValue:sensitivity];
        
        if (![_activeEffects containsObject:effect]) {
            [_activeEffects addObject:effect];
            i++;
        }
    }
    [_effectCollectionView reloadData];
    [self reloadFiltersOnCamera];
}

- (void)importVideo {
    _imagePicker = [[UIImagePickerController alloc] init];
    _imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
    _imagePicker.delegate = self;
    [self presentViewController:_imagePicker animated:YES completion:nil];
}

- (void)clearStillImageSource {
    if (_stillImageSource) {
        [_stillImageSource removeAllTargets];
        _stillImageSource = nil;
        _inputImage = nil;
        for (Effect *filter in _activeEffects) {
            [[filter imageFilter] removeAllTargets];
        }
    }
}

#pragma mark - Layout State

- (void)updateVideoModeButton {
    [self clearStillImageSource];
    if (_moonShadowMode == VideoMode) {
        _captureModeLabel.text = @"VIDEO";
        _captureModeImage.image =  [UIImage imageNamed:VIDEO_MODE_IMAGE_NAME];
        [_captureButton setImage:[UIImage imageNamed:VIDEO_RECORDING_START_BUTTON] forState:UIControlStateNormal];
        [_switchCameraButton setImage:[UIImage imageNamed:SWITCH_CAMERA_IMAGE_NAME] forState:UIControlStateNormal];
        _switchCameraButton.layer.borderWidth = 0.0f;
        _imageFilterView.hidden = NO;
        [self playMovieAtUrl:nil];
    } else if (_moonShadowMode == ImageMode) {
        _captureModeLabel.text = @"PHOTO";
        _captureModeImage.image = [UIImage imageNamed:PHOTO_MODE_IMAGE_NAME];
        [_captureButton setImage:[UIImage imageNamed:CAMERA_CAPTURE_BUTTON] forState:UIControlStateNormal];
        [_switchCameraButton setImage:[UIImage imageNamed:SWITCH_CAMERA_IMAGE_NAME] forState:UIControlStateNormal];
        _switchCameraButton.layer.borderWidth = 0.0f;
        [self playMovieAtUrl:nil];
        _imageFilterView.hidden = NO;
    } else {
        _captureModeLabel.text = @"IMPORT";
        _captureModeImage.image = [UIImage imageNamed:LIBRARY_MODE_IMAGE_NAME];
        [_captureButton setImage:[UIImage imageNamed:CAMERA_CAPTURE_BUTTON] forState:UIControlStateNormal];
        _switchCameraButton.layer.borderWidth = 1.0f;
        _switchCameraButton.layer.borderColor = [[UIColor blackColor] CGColor];
        [self updateCameraRollThumbnail];
        _imageFilterView.hidden = YES;
    }
}

- (void)updateVideoTime {
    [[[_pipeline filters] lastObject] useNextFrameForImageCapture];
    _recordingProgresstime++;
    
    int seconds = _recordingProgresstime % 60;
    int minutes = _recordingProgresstime / 60.0;
    
    NSString *secondsString = seconds >= 10 ? [NSString stringWithFormat:@"%d",seconds] : [NSString stringWithFormat:@"0%d",seconds];
    NSString *minutesString = minutes >= 10 ? [NSString stringWithFormat:@"%d",minutes] : [NSString stringWithFormat:@"0%d",minutes];
    
    _videoTimerLabel.text = [NSString stringWithFormat:@"%@:%@",minutesString, secondsString];
}

- (IBAction)toggleControlsViewTapped:(id)sender {
    if (_selectedEffect) {
        [_infoView action:InfoViewSensitivityTapped];
    } else {
        [self flashEffectsCollectionView];
        return;
    }
    [self toggleControlViewShowing];
}

- (void)toggleControlViewShowing {
    [UIView animateWithDuration:0.25f delay:0.0f options:UIViewAnimationOptionAllowUserInteraction animations:^{
        if (!_controlsViewShowing) {
            _controlsViewYPosConstraint.constant = CGRectGetHeight(_bottomContainerView.bounds);
            _infoViewYPosConstraint.constant = CGRectGetHeight(_controlsView.bounds);
        } else {
            _controlsViewYPosConstraint.constant = -CGRectGetHeight(_bottomContainerView.bounds);
            _infoViewYPosConstraint.constant = 0;
        }
        [_controlsView layoutIfNeeded];
        _controlsViewShowing = !_controlsViewShowing;
    } completion:nil];
}

- (CGSize)outputSizeForOrientation {

    if (_moonShadowMode == ImportMediaMode) {
        if (_importedVideoResolution.height > _importedVideoResolution.width) {
            return CGSizeMake(_importedVideoResolution.height, _importedVideoResolution.width);
        }
        return _importedVideoResolution;
    }
    
    CGSize size;

    if (_highQualitySupported) {
        size = [[SettingsStore sharedInstance] videoQuality] ? CGSizeMake(720.0, 1280.0) : CGSizeMake(480.0, 640.0);
    } else {
        size = CGSizeMake(480.0, 640.0);
    }
    
    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            return CGSizeMake(size.height, size.width);
            break;
        default:
            return size;
            break;
    }
}

- (GPUImageRotationMode)rotationModeForOrientation {
    if (_moonShadowMode == ImportMediaMode) {
        if (_importedVideoResolution.height > _importedVideoResolution.width) {
            return kGPUImageRotateRight;
        } else {
            return kGPUImageNoRotation;
        }
    }
    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationLandscapeLeft:
            return kGPUImageRotateLeft;
            break;
        case UIDeviceOrientationLandscapeRight:
            return kGPUImageRotateRight;
            break;
        default:
            return kGPUImageNoRotation;
            break;
    }
}

- (UIImageOrientation)imageOutputOrientation {
    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationLandscapeLeft:
            return UIImageOrientationLeft;
            break;
        case UIDeviceOrientationLandscapeRight:
            return UIImageOrientationRight;
        default:
            return UIImageOrientationUp;
            break;
    }
    return UIImageOrientationUp;
}

- (void)updateImageViewConstaintsForCameraMode {
    if (_highQualitySupported) {
        if ([[SettingsStore sharedInstance] videoQuality] == VideoQualityHigh) {
            _imageViewYPosConstraint.constant = 0;
            [_imageFilterView layoutIfNeeded];
            _bottomContainerView.backgroundColor = [UIHelper transparentBackgroundColor];
        } else {
            _imageViewYPosConstraint.constant = CGRectGetHeight(_controlsView.bounds);
            [_imageFilterView layoutIfNeeded];
            _bottomContainerView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
            _bottomContainerView.backgroundColor = [UIHelper opaqueBackgroundColor];
        }
    }
}

- (void)showBottomControlView {
    _bottomContainerView.hidden = NO;
    [UIView animateWithDuration:0.25 animations:^{
        _bottomContainerView.alpha = 1.0f;
    }];
}

- (void)flashEffectsCollectionView {
    _effectCollectionView.backgroundColor = [UIColor whiteColor];
    [UIView animateWithDuration:0.25 animations:^{
        _effectCollectionView.backgroundColor = [UIColor clearColor];
    }];
}

- (void)updateCameraRollThumbnail {
    [_assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        [group setAssetsFilter:[ALAssetsFilter allVideos]];
        [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *alAsset, NSUInteger index, BOOL *innerStop) {
            if (alAsset) {
                UIImage *latestPhoto = [UIImage imageWithCGImage:[alAsset thumbnail]];
                *stop = YES;
                *innerStop = YES;
                [_switchCameraButton setImage:latestPhoto forState:UIControlStateNormal];
            }
        }];
    } failureBlock: ^(NSError *error) {
        NSLog(@"No groups");
    }];
}

#pragma mark - UICollectionView

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"EffectCell";
    
    EffectCell *cell = (EffectCell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
    Effect *effect = _effects[indexPath.item];
    cell.titleLabel.text = effect.title;
    cell.imageView.image = effect.previewImage;
    cell.effect = effect;
    
    if (effect == _selectedEffect) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor blackColor];
    }
    
    if ([_activeEffects containsObject:effect]) {
        cell.selectedEffectImageView.image = [UIImage imageNamed:@"checkmark"];
    } else {
        cell.selectedEffectImageView.image = nil;
    }
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_effects count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [_infoView action:InfoViewEffectSelect];
    
    if (_isRecording && ![_activeEffects containsObject:_effects[indexPath.item]]) {
        return;
    }
    
    if (_selectedEffect == _effects[indexPath.item]) {
        [_activeEffects removeObject:_selectedEffect];
        _selectedEffect = nil;
        if (_controlsViewShowing) {
            [self toggleControlViewShowing];
        }
    } else {
        _selectedEffect = _effects[indexPath.item];
        if (![_activeEffects containsObject:_selectedEffect]) {
            [_activeEffects addObject:_selectedEffect];
        }
        _intensitySlider.value = [_selectedEffect getIntensity];
        [_blendModeControl setSelectedSegmentIndex:_selectedEffect.blendType];
    }
    
    _colorView.backgroundColor = _selectedEffect.currentColor;

    [_effectCollectionView reloadData];
    
    if (!_isRecording) {
        [self reloadFiltersOnCamera];
    }

}

#pragma mark - Image sampling

- (void)selectImagePoint:(UITapGestureRecognizer *)gesture {
    
    if (!_loadingOverlay.hidden) {
        return;
    }
    
    if (_selectedEffect) {
        [_infoView action:InfoViewImageTapped];
    } else {
        [self flashEffectsCollectionView];
        return;
    }
    
    __block CGPoint point = [gesture locationInView:_imageFilterView];
    __unsafe_unretained GPUImageRawDataOutput *rawOutput = _rawDataOutput;
    __unsafe_unretained Effect *effect = _selectedEffect;
    __unsafe_unretained GPUImageView *imageView = _imageFilterView;
    __unsafe_unretained UIView *colorView = _colorView;
    
    [_selectedEffect setTouchPos:CGPointMake(point.x/_imageFilterView.frame.size.width, point.y/_imageFilterView.frame.size.height)];
    
    [_rawDataOutput setNewFrameAvailableBlock:^{
        if (!CGPointEqualToPoint(point, CGPointZero)) {
            
            CGSize maxOutputSize = [rawOutput maximumOutputSize];
            CGSize currentViewSize = imageView.bounds.size;
            float avgR, avgG, avgB, avgA;
            avgR = avgG = avgB = avgA = 0.0f;
            CGPoint scaledPoint;
            scaledPoint.x = (point.x / currentViewSize.width) * maxOutputSize.width;
            scaledPoint.y = maxOutputSize.height - (point.y / currentViewSize.height) * maxOutputSize.height;
            
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    CGPoint samplePoint = CGPointMake(MIN(MAX(scaledPoint.x + i, 0), maxOutputSize.width),
                                                      MIN(MAX(scaledPoint.y + j, 0), maxOutputSize.height));
                    
                    GPUByteColorVector sampleColor = [rawOutput colorAtLocation:samplePoint];
                    avgR += (int)sampleColor.red/255.0;
                    avgG += (int)sampleColor.green/255.0;
                    avgB += (int)sampleColor.blue/255.0;
                    avgA += (int)sampleColor.alpha/255.0;
                }
            }
            
            avgR /= 9.0f;
            avgG /= 9.0f;
            avgB /= 9.0f;
            avgA /= 9.0f;
            
            point = CGPointZero;
            
            if (effect) {
                runOnMainQueueWithoutDeadlocking(^{
                    UIColor *avgColor = [UIColor colorWithRed:avgR green:avgG blue:avgB alpha:avgA];
                    [effect setCurrentColor:avgColor];
                    colorView.backgroundColor = avgColor;
                });
            }
        }
    }];
}

#pragma mark - Motion

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if ([_activeEffects count] && !_isRecording && motion == UIEventSubtypeMotionShake) {
        runOnMainQueueWithoutDeadlocking(^{
            if ([[CheatStore sharedInstance] isClearGlitchEnabled]) {
                _clearContextAlertView = [[UIAlertView alloc] initWithTitle:@"Clear Context" message:@"Would you like to clear all active effects?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Clear", nil];
                [_clearContextAlertView show];
                
            } else if (![[CheatStore sharedInstance] isClearGlitchUnlocked]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cheat code?" message:@"Perhaps if you enter \"milkshake\" at the cheat menu, you can use this feature!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }
        });
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView == _clearContextAlertView && buttonIndex != alertView.cancelButtonIndex) {
        runOnMainQueueWithoutDeadlocking(^{
            for (Effect *effect in _activeEffects) {
                [effect updateSensitivityWithValue:EFFECT_START_VAL];
            }
            [_activeEffects removeAllObjects];
            [self reloadFiltersOnCamera];
            [_effectCollectionView reloadData];
        });
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    _imageFilterView.hidden = NO;
    [self clearStillImageSource];
    if ([type isEqualToString:(NSString *)kUTTypeVideo] || [type isEqualToString:(NSString *)kUTTypeMovie]) {
        _importedVideoUrl = [info objectForKey:UIImagePickerControllerMediaURL];
        _importedVideoResolution = [self assetResolutionAtUrl:_importedVideoUrl];
        [self playMovieAtUrl:_importedVideoUrl];
        [self reloadFiltersOnCamera];
    } else {
        [_playingMovie removeAllTargets];
        [_playingMovie cancelProcessing];
        _playingMovie = nil;
        _player = nil;
        _playerItem = nil;
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        _inputImage = [UIHelper imageWithImage:image scaledToMaxWidth:1024 maxHeight:1024];
        [self reloadFiltersOnStillImageAndStartTimer:YES];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (CGSize)assetResolutionAtUrl:(NSURL *)url {
    
    AVAssetTrack *videoTrack;
    AVURLAsset *asset = [AVAsset assetWithURL:url];
    NSArray *videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
 
    if ([videoTracks count] > 0) {
        videoTrack = [videoTracks objectAtIndex:0];
    }
    
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if ((size.width == txf.tx && size.height == txf.ty) || (txf.tx == 0 && txf.ty == 0)) {
        return size;
    } else {
        return CGSizeMake(size.height, size.width);
    }
}

#pragma mark - GPUImageMovieDelegate

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    if (_isRecording) {
        [self finishRecording];
    } else {
        AVPlayerItem *item = [notification object];
        [item seekToTime:kCMTimeZero];
    }
}

- (void)didCompletePlayingMovie {
    [_movieWriter endProcessing];
    [[[_pipeline filters] firstObject] removeTarget:_movieWriter];
    [_movieWriter finishRecordingWithCompletionHandler:^{
        runOnMainQueueWithoutDeadlocking(^{
            [_captureButton setImage:[UIImage imageNamed:VIDEO_RECORDING_START_BUTTON] forState:UIControlStateNormal];
            [_loadingOverlay hide];
            _switchCameraButton.hidden = NO;
            _stillImageExportView.hidden = NO;
            _bottomContainerView.hidden = YES;
            [self playMovieAtUrl:[self moviePath]];
            _movieWriter = nil;
            _exportingMovie = nil;
        });
    }];
}

@end
