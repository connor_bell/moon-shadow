//
//  SocialUtil.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-07-24.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "SocialUtil.h"
#import <UIKit/UIKit.h>

@implementation SocialUtil

+ (void)openUserInstagram:(NSString *)username {
    NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://user?username=%@",username]];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [[UIApplication sharedApplication] openURL:instagramURL];
    } else {
        NSURL *webUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://instagram.com/%@", username]];
        [[UIApplication sharedApplication] openURL:webUrl];
    }
}

+ (void)openUserTwitter:(NSString *)username {
    NSURL *twitterUrl = [NSURL URLWithString: [NSString stringWithFormat:@"twitter:///user?screen_name=%@", username]];
                                           
    if ([[UIApplication sharedApplication] canOpenURL:twitterUrl]){
        [[UIApplication sharedApplication] openURL:twitterUrl];
    } else {
        NSURL *webUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://twitter.com/%@", username]];
        [[UIApplication sharedApplication] openURL:webUrl];
   }
}

@end
