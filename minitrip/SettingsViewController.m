//
//  SettingsViewController.m
//  MOON SHADOW
//
//  Created by Connor Bell on 2015-04-23.
//  Copyright (c) 2015 Connor Bell. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsStore.h"
#import "SocialUtil.h"

static NSString *SEGUE_ID_VIDEO_QUALITY = @"SEGUE_ID_VIDEO_QUALITY";
static NSString *SEGUE_ID_CHEAT_CODES = @"SEGUE_ID_CHEAT_CODES";
static NSString *SEGUE_ID_CREDITS = @"SEGUE_ID_CREDITS";

static NSString *const MOON_SHADOW_EMAIL = @"moonshadowapp@gmail.com";
static NSString *const MOON_SHADOW_INSTAGRAM = @"moonshadowapp";
static NSString *const MOON_SHADOW_TWITTER = @"moonshadowapp";

@interface SettingsViewController () {
    MFMailComposeViewController *_mailController;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(dismissSettings)];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_audioEnabledSwitch setOn:[[SettingsStore sharedInstance] microphoneAudioEnabled]];
    [_touchSampleEnabledSwitch setOn:[[SettingsStore sharedInstance] touchSamplesOriginalImage]];

    if ([[SettingsStore sharedInstance] videoQuality] == VideoQualityHigh) {
        _selectedVideoSettingLabel.text = @"HIGH";
    } else {
        _selectedVideoSettingLabel.text = @"LOW";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dismissSettings {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)audioSwitchTapped:(id)sender {
    [[SettingsStore sharedInstance] setMicrophoneAudioEnabled:_audioEnabledSwitch.isOn];
}

- (IBAction)touchSamplePosTapped:(id)sender {
    [[SettingsStore sharedInstance] setTouchSamplesOriginalImage:_touchSampleEnabledSwitch.isOn];
}

- (void)emailSupport {
    if ([MFMailComposeViewController canSendMail]) {
        _mailController = [MFMailComposeViewController new];
        _mailController.delegate = self;
        _mailController.mailComposeDelegate = self;
        [_mailController setToRecipients:@[MOON_SHADOW_EMAIL]];
        [self presentViewController:_mailController animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Support"
                                                        message:@"You can email me at moonshadowapp@gmail.com"
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

#pragma mark - UITableViewDelegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 2) { // maek_better
        [self performSegueWithIdentifier:SEGUE_ID_VIDEO_QUALITY sender:nil];
    }
    if (indexPath.section == 1 && indexPath.row == 0) {
        [self performSegueWithIdentifier:SEGUE_ID_CHEAT_CODES sender:nil];
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        [self performSegueWithIdentifier:SEGUE_ID_CREDITS sender:nil];
    }
    if (indexPath.section == 1 && indexPath.row == 2) {
        [self emailSupport];
    }
    if (indexPath.section == 1 && indexPath.row == 3) {
        [SocialUtil openUserInstagram:MOON_SHADOW_INSTAGRAM];
    }
    if (indexPath.section == 1 && indexPath.row == 4) {
        [SocialUtil openUserTwitter:MOON_SHADOW_TWITTER];
    }
}

#pragma mark - MFMailComposeDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [_mailController dismissViewControllerAnimated:YES completion:nil];
}

@end
